library("parallel")

make_cross_val_splits<-function(list_of_datasets_to_combine, fold, starter_seed, dataset_dir, force = FALSE){
    out_list_of_datasets_to_combine<-list()
    for (type_of_analysis in names(list_of_datasets_to_combine)){
        temp_analysis <- list_of_datasets_to_combine[[type_of_analysis]]
        temp_all_datasets<-temp_analysis[["DATASET_LIST"]]
        temp_all_short_names <- temp_analysis[["SHORT_NAMES"]]
        ################################
        ## do the splits
        list_of_out_dir_names<-c()
        list_of_short_names<-c()
        for (temp_file_index in 1:length(temp_analysis[["DATASET_LIST"]])){
            temp_in_file <- paste(dataset_dir,temp_all_datasets[temp_file_index],sep='/')
            temp_base_short_name <- temp_all_short_names[temp_file_index]
            list_of_short_names<-c(list_of_short_names,temp_base_short_name)
            list_of_out_dir_names<-c(list_of_out_dir_names,temp_base_short_name)
            temp_out_dir <- paste(dataset_dir,temp_base_short_name,sep='/')
            temp_out_name <- temp_all_datasets[temp_file_index]
            dummy_file <- paste(temp_out_dir,paste(temp_base_short_name,"0.tsv",sep="_"),sep='/')
            if (continue_func(force, dummy_file)){
                cv_call_vect<-c("python3 -m pyminer_norm.split_dataset_for_cv -i", temp_in_file,
                                "-o", temp_out_dir,
                                "-fold", fold,
                                "-seed", starter_seed,
                                "-name",temp_base_short_name)
                cv_call<-paste(cv_call_vect,collapse = " ")
                print(cv_call)
                system(cv_call)
                dataset_paths<-c()
                short_names<-c()
                for (temp_fold in 1:fold){
                    dataset_paths <- c(dataset_paths,paste(c(temp_out_dir,'/',temp_fold,'.tsv'),sep=''))
                    short_names<-c(short_names,paste(temp_base_short_name,temp_fold,sep='_'))
                }
            }
        }
        ##################################
        ## catalogue the new list of lists to combine
        for (f in 1:fold){
            temp_all_datasets<-c()
            temp_all_short_names_final<-c()
            for (i in 1:length(list_of_out_dir_names)){
                temp_dataset<-list_of_out_dir_names[i]
                cur_short_name<-paste(list_of_short_names[i],f-1,sep='_')
                dummy_file <- paste(c(temp_dataset,'/',cur_short_name,".tsv"),collapse='')
                temp_all_datasets<-c(temp_all_datasets,dummy_file)
                temp_all_short_names_final<-c(temp_all_short_names_final,cur_short_name)
            }
            temp_run_name<-paste(type_of_analysis,f,sep='_')
            out_list_of_datasets_to_combine[[temp_run_name]]<-list()
            out_list_of_datasets_to_combine[[temp_run_name]][["DATASET_LIST"]]<-temp_all_datasets
            out_list_of_datasets_to_combine[[temp_run_name]][["SHORT_NAMES"]]<-temp_all_short_names_final
            ## now log everything else
            for (thing in names(list_of_datasets_to_combine[[type_of_analysis]])){
                if (thing %!in% c("DATASET_LIST","SHORT_NAMES")){
                    out_list_of_datasets_to_combine[[temp_run_name]][[thing]]<-list_of_datasets_to_combine[[type_of_analysis]][[thing]]
                }
            }
        }
    }
    print(out_list_of_datasets_to_combine)
    return(out_list_of_datasets_to_combine)
}



get_dataset_strs<-function(in_dir, datasets){
    return(paste(in_dir, datasets,sep='/'))
}


load_dataset2<-function(data_file,short_name="NA", load_sparse = FALSE){
    ## read in the full dataset and make it a matrix
    #temp_table<-read.csv(data_file, sep='\t',header=TRUE)
    temp_table<-load_single_dset(data_file)
    ncol<-dim(temp_table)[2]
    full_expression_mat <- as.matrix(temp_table[,2:ncol])
    rownames(full_expression_mat)<-temp_table[,1]
    if (short_name != "NA"){
        colnames(full_expression_mat) <- paste(short_name,colnames(full_expression_mat),sep='||')
    }
    if (load_sparse){
        full_expression_mat<-Matrix(full_expression_mat, sparse=TRUE)
    }
    gc()
    return(full_expression_mat)
}


load_dataset<-function(data_file,short_name="NA"){
    ## read in the full dataset and make it a matrix
    #temp_table<-read.csv(data_file, sep='\t',header=TRUE)
    temp_table<-load_single_dset(data_file)
    ncol<-dim(temp_table)[2]
    #full_expression_mat <- as.matrix(temp_table[,2:ncol])
    rownames(temp_table)<-temp_table[,1]
    if (short_name != "NA"){
        colnames(temp_table) <- paste(short_name,colnames(temp_table),sep='||')
    }
    colnames(temp_table)[1]<-'gene'
    return(temp_table)
}


pre_select_genes<-function(dataset_names,local_short_names){

}


load_single_dset<-function(dset_file){
    possible_rds<-gsub(".tsv",".Rds",dset_file)
    if (file.exists(possible_rds)){
        return(readRDS(possible_rds))
    } else {
        if (file.exists(dset_file)){
            temp_data<-read.csv(dset_file, sep='\t',header=TRUE)
            saveRDS(temp_data, file=possible_rds)
            return(temp_data)
        } else {
            message("COULDN'T FIND FILE: ", dset_file)
            return(NULL)
        }
    }
}


load_concatenated_datasets<-function(dataset_names,local_short_names, load_sparse=FALSE){
    message("loading concatenated datasets; sparse:",load_sparse)
    for (i in 1:length(dataset_names)){
        print(paste("    loading",dataset_names[i],sep=' '))
        if (i==1){
            in_mat <- load_dataset(dataset_names[i])
            #in_mat <- load_single_dset(dataset_names[i])
            colnames(in_mat) <- paste(local_short_names[i],colnames(in_mat),sep='||')
            colnames(in_mat)[1] <- 'gene'
            rownames(in_mat)<-in_mat[,1]
            print(colnames(in_mat)[1:5])
            batch <- rep(local_short_names[i],length(colnames(in_mat))-1)
            if (load_sparse){
                message("WARNING: loading sparse assumes that all have the same reference")
                rownames(in_mat)<-in_mat[,1]
                in_mat<-in_mat[,2:length(colnames(in_mat))]
                in_mat <- Matrix(as.matrix(in_mat),sparse=TRUE)
            }
        } else {
            temp_mat <-load_dataset(dataset_names[i])
            #temp_mat <-load_single_dset(dataset_names[i])
            colnames(temp_mat) <- paste(local_short_names[i],colnames(temp_mat),sep='||')
            colnames(temp_mat)[1] <- 'gene'
            print(colnames(temp_mat)[1:5])
            if (load_sparse){
                temp_mat <- Matrix(as.matrix(temp_mat[,2:length(colnames(temp_mat))]),sparse=TRUE)
            }
            if (load_sparse){
                in_mat <-  Matrix(cbind(in_mat, temp_mat),sparse=TRUE)
            } else {
                in_mat<-merge(in_mat,temp_mat, by = "gene", all.x = TRUE)
            }
            # if (load_sparse){
            #     in_mat <- Matrix(as.matrix(in_mat),sparse=TRUE)
            # }
            print(class(in_mat))
            #in_mat <- cbind(in_mat, temp_mat)
            if (load_sparse){
                batch <- c(batch, rep(local_short_names[i],length(colnames(temp_mat))))
            } else {
                batch <- c(batch, rep(local_short_names[i],length(colnames(temp_mat))-1))
            }
            gc()
        }
        print(dim(in_mat))
    }
    in_mat[is.na(in_mat)] <- 0
    rownames(in_mat) <- in_mat[,1]
    ncol <- dim(in_mat)[2]
    if (load_sparse){
        in_mat <- Matrix(in_mat,sparse=TRUE)
        gc()
    } else {
        in_mat <- as.matrix(in_mat[,2:ncol])
    }
    batch <- as.factor(batch)
    print(in_mat[1:5,1:5])
    print(batch[1:5])
    print(dim(in_mat))
    print(length(batch))
    gc()
    return(list(in_mat, batch))
}


finished_norm<-function(in_dir, log2_name = FALSE, hdf5_name = FALSE){
    base_name<-paste(in_dir,'normalized_input',sep='/')
    if (log2){
        base_name <- paste(base_name,'log2',sep = "_")
    }
    if (hdf5){
        base_name <- paste(base_name,'hdf5',sep='.')
    } else {
        base_name <- paste(base_name,'tsv',sep='.')
    }
    print("checking for:")
    print(base_name)
    finished_clust_bool <- file.exists(base_name)
    finished_clust_bool <- finished_clust_bool + file.exists(paste(base_name,".gz",sep=""))
    if (!(force | !finished_clust_bool)) {
        print("    already done & not forcing")
        return(TRUE)
    } else {
        return(FALSE)
    }
}


get_variable_features_df<-function(exprs){
    object <- CreateSeuratObject(exprs)
    object<-NormalizeData(object, verbose = FALSE)
    object <- FindVariableFeatures(object)
    variable_feature_df <- HVFInfo(object, status=TRUE)#[VariableFeatures(object),]
    return(variable_feature_df)
}


get_seurat_variable_features<-function(exprs){
    variable_feature_df <- get_variable_features_df(exprs)
    var_genes<-unique(rownames(variable_feature_df)[which(variable_feature_df$variable == TRUE)])
    return(var_genes)
}


## </helper functions>
############################



############################
## <clustering call>


do_gprofiler<-function(wd, species, force = F){
    id_list <- paste(wd, 'ID_list.txt',sep='/')
    out_file <- paste(wd, 'annotations',sep='/')
    gprofiler_call <- c("pyminer_gprofiler_converter.py -i ",id_list, " -o ",out_file," -s ",species," -annotations")

    finished_clust_bool <- file.exists(paste(out_file,'.tsv',sep='/'))
    if (!(force | !finished_clust_bool)) {
        print("already done gprofiler & not forcing")
    } else {
        gprofiler_call<-paste(gprofiler_call, collapse = ' ')
        system(gprofiler_call)
    }

    if (species != 'hsapiens'){
        human_orthologues <- paste(wd,"human_orthologues",sep='/')
        finished_clust_bool <- file.exists(paste(human_orthologues,'.tsv',sep='/'))
        if (!(force | !finished_clust_bool)) {
            print("already done gprofiler ortholgoues & not forcing")
        } else {
            gprofiler_call <- c("pyminer_gprofiler_converter.py -i ",id_list," -o ", human_orthologues,"-s ",species)
            gprofiler_call<-paste(gprofiler_call, collapse = ' ')
            system(gprofiler_call)
        }
    }
}


do_clustering<-function(in_dir, species, clust_on_all = FALSE, no_norm=FALSE, anti = FALSE, mono = FALSE, cell_subset = 1:NUM_CELLS, force = F, global_feat_select_already_done=FALSE,do_pca=FALSE,skip_spear=FALSE){
    if (global_feat_select_already_done){
        ## this overrides & we'll do the clustering on all option
        clust_on_all = TRUE
    }
    dest_clust_file<-paste(in_dir,'sample_clustering_and_summary/sample_k_means_groups.tsv',sep='/')
    finished_clust_bool <- file.exists(dest_clust_file)
    print("#############################################")
    print(finished_clust_bool)
    print("#############################################")
    if (!(force | !finished_clust_bool)) {
        print("already done & not forcing")
        return()
    }
    infile<-paste(in_dir,'normalized_counts_log2.hdf5',sep='/')
    ID_list<-paste(in_dir,'ID_list.txt',sep='/')
    cols <- paste(in_dir,'column_IDs.txt',sep='/')
    do_gprofiler(in_dir, species, force = F)
    if (!clust_on_all){
        disp_dir<-paste(in_dir,"sample_clustering_and_summary/dispersion/",sep="/")
        dir.create(disp_dir, recursive=TRUE, showWarnings=FALSE)
        disperstion_call <- c("python3 -m pyminer.pyminer_get_dispersion -hdf5 -i ",infile," -out_dir ",disp_dir, " -ids ",ID_list)
        disperstion_call <- paste(disperstion_call,collapse=" ")
        print(disperstion_call)
        system(disperstion_call)
        clust_gene_file<-paste(disp_dir,"locally_overdispersed_boolean_table.txt",sep="")
    } else {
        clust_gene_file<-ID_list
    }
    disp_clust_call<-c("python3 -m pyminer.clustering -no_plots -leave_mito_ribo -hdf5 -i ",infile," -species ",species," -ID_list ",ID_list," -columns ",cols," -clust_on_genes ",clust_gene_file," -louvain_clust -merge ")
    if (do_pca){
        disp_clust_call<-c(disp_clust_call, " -pca_reduce ")
    }
    if (skip_spear){
        disp_clust_call<-c(disp_clust_call, " -skip_spearman ")
    }
    if (no_norm){
        disp_clust_call<-c(disp_clust_call," -no_var_norm ")
    } else {
        disp_clust_call<-c(disp_clust_call," -var_norm -rank ")
    }
    disp_clust_call<-paste(disp_clust_call, collapse=" ")
    print(disp_clust_call)
    system(disp_clust_call)
}



do_clustering_with_pca_euc_knn_louvain<-function(in_dir, species, clust_on_all = FALSE, no_norm=FALSE, anti = FALSE, mono = FALSE, cell_subset = 1:NUM_CELLS, force = F){
    ## TODO: actually write this function
    dest_clust_file<-paste(in_dir,'sample_clustering_and_summary/sample_k_means_groups.tsv',sep='/')
    finished_clust_bool <- file.exists(dest_clust_file)
    print("#############################################")
    print(finished_clust_bool)
    print("#############################################")
    if (!(force | !finished_clust_bool)) {
        print("already done & not forcing")
        return()
    }
    
    infile<-paste(in_dir,'normalized_counts_log2.hdf5',sep='/')
    ID_list<-paste(in_dir,'ID_list.txt',sep='/')
    cols <- paste(in_dir,'column_IDs.txt',sep='/')
    do_gprofiler(in_dir, species, force = F)
    if (!clust_on_all){
        disp_dir<-paste(in_dir,"sample_clustering_and_summary/dispersion/",sep="/")
        dir.create(disp_dir, recursive=TRUE, showWarnings=FALSE)
        disperstion_call <- c("python3 -m pyminer.pyminer_get_dispersion -hdf5 -i ",infile," -out_dir ",disp_dir, " -ids ",ID_list)
        disperstion_call <- paste(disperstion_call,collapse=" ")
        print(disperstion_call)
        system(disperstion_call)
        clust_gene_file<-paste(disp_dir,"locally_overdispersed_boolean_table.txt",sep="")
    } else {
        clust_gene_file<-ID_list
    }
    disp_clust_call<-c("python3 -m pyminer.clustering -no_plots -leave_mito_ribo -hdf5 -i ",infile," -species ",species," -ID_list ",ID_list," -columns ",cols," -clust_on_genes ",clust_gene_file," -louvain_clust -merge ")
    if (no_norm){
        disp_clust_call<-c(disp_clust_call," -no_var_norm ")
    } else {
        disp_clust_call<-c(disp_clust_call," -var_norm -rank ")
    }
    disp_clust_call<-paste(disp_clust_call, collapse=" ")
    print(disp_clust_call)
    system(disp_clust_call)
}

## </clustering call>
#############################################
################################################





convert_reference<-function(gene,gene_ref_list){
    return(gene_ref_list[[gene]])
}


concatenate_already_loaded_datasets<-function(dataset_lists, 
                                              local_short_names, 
                                              load_sparse=FALSE){
    num_dsets<-length(dataset_lists)
    num_cols<-0
    all_genes<-c()
    column_offset<-c(0)
    row_genes<-c()
    col_vect<-c()
    vals<-c()
    all_colnames<-c()
    batch<-c()
    message("  -beginning dataset loading:")
    ## first we'll concatenate the dataset names to the cell names & 
    for (i in seq(num_dsets)){
        ## sparse matrices use zero indexing!!!
        message(paste("    loading:",local_short_names[[i]],sep=' '))
        all_colnames<-c(all_colnames,paste(local_short_names[i],
                                          colnames(dataset_lists[[i]]), sep="||"))
        all_genes<-sort(unique(c(all_genes, rownames(dataset_lists[[i]]))))
        batch <- c(batch, rep(local_short_names[i], length(colnames(dataset_lists[[i]]))))
        ##
        temp_mat<-as(dataset_lists[[i]],"dgTMatrix")
        temp_row_genes<-rownames(dataset_lists[[i]])[temp_mat@i+1]## plus 1 b/c of zero indexing
        row_genes<-c(row_genes,temp_row_genes)
        #col_vect<-c(col_vect,temp_mat@j+num_cols)
        col_vect<-c(col_vect,temp_mat@j+column_offset[length(column_offset)])
        vals<-c(vals,temp_mat@x)
        num_cols<-num_cols+length(colnames(dataset_lists[[i]]))
        column_offset<-c(column_offset, num_cols)
    }
    message("    found a total of ",length(all_colnames)," cells")
    #####################################
    ## update the rows to the indices
    message("  -converting to unified gene reference")
    gene_ref_list<-list()
    for( i in seq(1,length(all_genes))){
        gene_ref_list[[all_genes[i]]]<-i-1## -1 b/c of zero indexing
    }
    cl <- makeCluster(detectCores())
    #row_gene_idxs<-lapply(row_genes,convert_reference, gene_ref_list=gene_ref_list)
    row_gene_idxs<-unlist(parLapply(cl, row_genes, convert_reference, gene_ref_list=gene_ref_list))
    stopCluster(cl)
    #####################################
    message("  -creating output matrix")
    message("      ",c(length(all_genes)," x ", num_cols))
    out_mat<-as(sparseMatrix(i=row_gene_idxs+1,## ugh - internal representation is zero indexed
                             j=col_vect+1,## but it expects these to be 1 indexed
                             x=vals,
                             dims=c(length(all_genes), num_cols)  ),
                "dgCMatrix")
    colnames(out_mat)<-all_colnames
    rownames(out_mat)<-all_genes
    batch <- as.factor(batch)
    gc()
    return(list(out_mat, batch))
}

