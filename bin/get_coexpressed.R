library("Matrix")
library("dplyr")
library("parallel")

is_better_than_null_result<-function(in_vect, goi_vect, null_df){
	if (sum(in_vect)==0){
		return(c(FALSE,0))
	}
	real_cor<-cor(in_vect, goi_vect)
	null_bg<-apply(null_df,1,cor, y=in_vect)
	if (abs(real_cor)>max(abs(null_bg))){
		return(c(TRUE, real_cor))
	} else {
		return(c(FALSE, real_cor))
	}
}


get_null_df<-function(in_vect, iters=20){
	null_df<-matrix(0,ncol=length(in_vect),nrow=iters)
	for (i in seq(1,dim(null_df)[1])){
		null_df[i,]<-in_vect
	}
	null_df<-t(apply(null_df,1,sample))
	return(null_df)
}


apply_is_better_than_null_result<-function(in_vect, goi_vect, null_df){
	in_vect<-as.numeric(in_vect)
	goi_vect<-as.numeric(goi_vect)
	print(head(in_vect))
	print(head(goi_vect))
	print(sum(in_vect))
	if (sum(in_vect)==0){
		return(c(FALSE,0))
	}
	real_cor<-cor(in_vect, goi_vect)
	null_bg<-c()
	tryCatch(expr={
			null_bg<-apply(null_df,1,cor, y=in_vect)	
		},
		error=function(e){
			for (b in dim(null_df)[1]){
				message("null_bg: ", paste(null_bg))
				null_bg<-c(null_bg,cor(as.numeric(null_df[b,]),in_vect ) )
			}
		})
	if (abs(real_cor)>max(abs(null_bg))){
		return(list(is_better_than_null=TRUE, 
			        cor_val=real_cor))
	} else {
		return(list(is_better_than_null=FALSE, 
			        cor_val=real_cor))
	}
}


reformat_list_cor_res_to_df<-function(list_res, only_keep_significant=FALSE){
	gene_name<-names(list_res)
	better_than_null<-rep(FALSE,length(gene_name))
	cor_vals<-rep(0,length(gene_name))
	out_df<-data.frame(gene_name=as.character(gene_name),
		               is_better_than_null=better_than_null,
		               cor_vals=cor_vals)
	rownames(out_df)<-gene_name
	for(i in seq(1,length(gene_name))){
		temp_res<-unlist(list_res[gene_name[i]])
		out_df[gene_name[i],"is_better_than_null"]<-as.logical(temp_res[1])
		out_df[gene_name[i],"cor_vals"]<-as.numeric(temp_res[2])
	}
	if (only_keep_significant){
		return(out_df[which(out_df$better_than_null),])
	} else {
		return(out_df)
	}
}



'%!in%' <- function(x,y)!('%in%'(x,y))


get_sparse_coexpressed<-function(in_df, 
	                             goi=NULL, 
	                             goi_vect=NULL, 
	                             alpha=0.001, 
	                             bin_size=5000, 
	                             only_keep_significant=FALSE,
	                             do_rank=TRUE,
	                             quiet=FALSE){
	##  Takes in the dataframe and the gene of interest & returns the list of genes that are significantly 
	##  coexpressed above the alpha, as determined exclusively by a bootstrap shuffled 
	## first we'll just dense_rank transform everything, so that we don't 
	## have to do it over and over to save on computational overhead for Spearman
	start <-1
	end <- dim(in_df)[1]
	## make sure we keep the original names
	keep_names<-rownames(in_df)[seq(start,end)]
	#in_df[seq(start,end),]<-as(Matrix(t(apply(in_df[seq(start,end),],1,dense_rank))-1,sparse=TRUE),"RsparseMatrix")
	if (do_rank){
		if (!quiet){
			message("    Rank transforming for Spearman")
		}

		tryCatch(
		        expr = {
		            in_df<-as(Matrix(t(apply(in_df[seq(start,end),],1,dense_rank) )-1,sparse=TRUE),"RsparseMatrix")
		            # Your code...
		            # goes here...
		            # ...
		        },
		        error = function(e){
		            message("error b/c it's too big")
		            for (i in dim(in_df)[1]){
		            	in_df[i,]<-dense_rank(in_df[i,])-1
		                
		            }
		            # (Optional)
		            # Do this if an error is caught...
		        }
		    )

		
	} else {
		temp_class<-c(unlist(class(in_df)))
		if ("dgTMatrix" %!in% temp_class){
			in_df<-as(Matrix(in_df[seq(start,end),], sparse=TRUE), "RsparseMatrix")
		} else {
			in_df<-in_df[seq(start,end),]
		}
	}
	rownames(in_df)<-keep_names
	if (is.null(goi_vect) & !is.null(goi)){
		## as an alternative, the user could supply 
		goi_vect<-as.numeric(in_df[goi,])
	}
	if (!quiet){
		message("    getting the null vectors")
	}
	if (is.null(goi_vect) & !is.null(goi)){
		null_df<-get_null_df(in_df[goi,],iters = ceiling(1/alpha))
	} else {
		null_df<-get_null_df(goi_vect,iters = ceiling(1/alpha))
	}
	sig_df<-data.frame(gene=NULL,
		               is_better_than_null=NULL,
		               rho=NULL)
	if (!quiet){
		message("    getting all correlations")
	}
	#all_results<-apply(in_df, 1, apply_is_better_than_null_result, goi_vect=goi_vect, null_df=null_df)
	#### trying to make parallel b/c it's taking too long
	cl <- makeCluster(detectCores())
	#clusterExport(cl, c("sparse_cor"))
	tryCatch(
		expr = {
			all_results <- parApply(cl, X=in_df, MARGIN=1, FUN=apply_is_better_than_null_result, goi_vect=goi_vect, null_df=null_df)#, chunk.size=5000)
		},
		error=function(e){
			null_df<-as(Matrix(null_df, sparse=TRUE), "RsparseMatrix")
			print("dim(null_df)")
			print(dim(null_df))
			gc()
			all_results <- list()
			interval<-50
			num_rows<-dim(in_df)[1]
			for (i in seq(,by=interval)){
				from<-i
				to<-min(c(i+interval,num_rows))
				offset<-from-1
				message("    ",from," to ",to)
				temp_in_df <- as.matrix(in_df[seq(from,to),])
				print(dim(temp_in_df))
				temp_results <- parApply(cl, X=temp_in_df, MARGIN=1, FUN=apply_is_better_than_null_result, goi_vect=goi_vect, null_df=null_df)
				for (j in names(temp_results)){
					print(j)
					all_results[[j+offset]]<-temp_results[[j]]
				}
				# all_results[[i]]<-apply_is_better_than_null_result(in_df[i,],
				# 	                                               goi_vect=goi_vect,
				# 	                                               null_df=null_df)
			}
		}
	)
	stopCluster(cl)
	#ans <- Reduce("cbind", matrix_of_sums)
	all_results<-reformat_list_cor_res_to_df(all_results)
	return(all_results)
}


get_pathways_from_cor_df<-function(cor_df, remove_goi, species="human"){
	gprof_species_db_lookup<-list()
	gprof_species_db_lookup[["human"]]<-"hsapiens"
	gprof_species_db_lookup[["mouse"]]<-"mmusculus"
	message("getting pathway analysis")
	temp_query=as.character(unlist(cor_df[which(cor_df$is_better_than_null & cor_df[,1] %!in% remove_goi),1]))
	if (length(temp_query)==0){
		print("empty query")
		return(NULL)
	}
	cor_res=gost(query=temp_query,
		         organism=gprof_species_db_lookup[[species]],
		         evcodes=TRUE,
		         custom_bg=as.character(cor_df[,1]))
	if (is.null(cor_res)){
		return(NULL)
	} else {
		p_vect<-unlist(cor_res$result[,"p_value"])
		cor_res<-cor_res$result[order(p_vect),]
	}
	return(cor_res)
}




get_edge_ids<-function(edge_df){
	out_edge_ids<-c()
	for (i in seq(dim(edge_df)[1])){
		node_list<-sort(as.character(unlist(edge_df[i,])))
		edge_id<-paste(node_list,collapse="||")
		#print(edge_id)
		out_edge_ids<-c(out_edge_ids,edge_id)
	}
	#print(out_edge_ids)
	return(out_edge_ids)
}




get_full_adj_list<-function(in_df, alpha=0.01, return_all=FALSE, do_rank=TRUE){
	orig_names<-rownames(in_df)
	if (do_rank){	
		in_df<-as(Matrix(
				             t(
				             	apply(in_df,1,dense_rank) )-1,
				             sparse=TRUE),
			          "RsparseMatrix")
		rownames(in_df)<-orig_names
	}
	num_genes<-dim(in_df)[1]
	out_adj<-NULL
	for (i in seq(1,num_genes-1)){
		gene_1<-rownames(in_df)[i]
		if (i%%20==0){
			message("            ",round(100*i/num_genes,2)," %")
		}
		temp_in_mat<-in_df[i:num_genes,]
		## We set do_rank to false in this call because we already did it above if that's what we're doing
		temp_res<-get_sparse_coexpressed(temp_in_mat, goi=gene_1, alpha=alpha, do_rank=FALSE, quiet=TRUE)
		if (!return_all){
			## remove the non-significants
			temp_res<-temp_res[which(temp_res[,"is_better_than_null"]==TRUE),]
			## and self correlations
			temp_res<-temp_res[which(temp_res[,"gene_name"]!=gene_1),]
		}
		gene_1<-rep(gene_1,dim(temp_res)[1])
		temp_res<-cbind(gene_1,temp_res)
		if (is.null(out_adj)){
			out_adj<-temp_res
		} else {
			out_adj<-rbind(out_adj,temp_res)
		}
	}
	edge_ids<-get_edge_ids(out_adj[,c(1,2)])
	out_adj<-as.data.frame(cbind(edge_ids,out_adj))
	colnames(out_adj)<-c("edge_id", "gene_1", "gene_2","is_better_than_null", "cor_vals")
	return(out_adj)
}

# adj<-get_full_adj_list(a[1:25,])



################################################################


get_parallel_cors<-function(index, all_comparison_pairs_df, in_df, alpha){
	# print(index)
	# print(dim(all_comparison_pairs_df))
	# print(class(in_df))
	# print(dim(in_df))
	gene_1<-as.character(all_comparison_pairs_df[index,1])
	gene_2<-as.character(all_comparison_pairs_df[index,2])
	print(gene_1)
	print(gene_2)
	print(head(rownames(in_df)))
	vect_1<-in_df[gene_1,]
	print(vect_1)
	vect_2<-in_df[gene_2,]
	print(vect_2)
	if (sum(vect_1)==0 || sum(vect_2)==0){
		return(list(gene_1=gene_1,gene_2=gene_2,
			        is_better_than_null=FALSE,
			        cor_vals=0))
	}
	null_df<-get_null_df(vect_2,iters = ceiling(1/alpha))
	print(dim(null_df))
	real_cor<-cor(vect_2, vect_1)
	print(real_cor)
	null_bg<-unlist(apply(null_df,1,cor, y=vect_1))
	print(head(null_bg))
	if (abs(real_cor)>max(abs(null_bg))){
		return(list(gene_1=gene_1,gene_2=gene_2,
			        is_better_than_null=TRUE, 
			        cor_vals=real_cor))
	} else {
		return(list(gene_1=gene_1,gene_2=gene_2,
			        is_better_than_null=FALSE, 
			        cor_vals=real_cor))
	}
}



reformat_list_cor_res_to_df2<-function(list_res, only_keep_significant=FALSE){
	num_compares<-length(list_res)
	better_than_null<-rep(FALSE,num_compares)
	cor_vals<-rep(0,num_compares)
	out_df<-data.frame(edge_id=rep(NA,num_compares),
		               gene_1=rep(NA,num_compares),
		               gene_2=rep(NA,num_compares),
		               is_better_than_null=better_than_null,
		               cor_vals=cor_vals)
	for(i in seq(1,num_compares)){
		out_df[i,"gene_1"]<-unlist(list_res[[i]]$gene_1)
		out_df[i,"gene_2"]<-unlist(list_res[[i]]$gene_2)
		genes<-sort(c(unlist(list_res[[i]]$gene_1),unlist(list_res[[i]]$gene_2)))
		out_df[i,"edge_id"]<-paste(genes,collapse="||")
		out_df[i,"is_better_than_null"]<-unlist(list_res[[i]]$is_better_than_null)
		out_df[i,"cor_vals"]<-unlist(list_res[[i]]$cor_vals)
	}
	if (only_keep_significant){
		return(out_df[which(out_df$better_than_null),])
	} else {
		return(out_df)
	}
}



get_full_adj_list_parallel<-function(in_df, alpha=0.01, return_all=FALSE, do_rank=TRUE){
	orig_names<-rownames(in_df)
	if (do_rank){	
		in_df<-as(Matrix(
				             t(
				             	apply(in_df,1,dense_rank) )-1,
				             sparse=TRUE),
			          "RsparseMatrix")
		rownames(in_df)<-orig_names
	}
	num_genes<-dim(in_df)[1]
	out_adj<-NULL
	g1<-c()
	g2<-c()
	for (i in seq(1,num_genes-1)){
		for (j in seq(i,num_genes-1)){
			if (i!=j){
				g1<-c(g1,rownames(in_df)[i])
				g2<-c(g2,rownames(in_df)[j])
			}
		}
	}
	comparisons_df<-data.frame(g1=as.character(g1),
		                       g2=as.character(g2),
		                       stringsAsFactors=FALSE)
	# print(head(comparisons_df))
	idx_vect<-matrix(seq(length(g1)),nrow=length(g1),ncol=1)
	cl <- makeCluster(detectCores())
	clusterExport(cl, c("get_null_df"), 
              envir=environment())
	# print(head(idx_vect))
	# print(head(comparisons_df))
	# print(class(in_df))
	# print(dim(in_df))
	# print(alpha)
	all_results <- parApply(cl, X=idx_vect, 
	                            MARGIN=1, 
	                            FUN=get_parallel_cors, 
	                            all_comparison_pairs_df=comparisons_df, 
	                            in_df=in_df, 
	                            alpha=alpha)#, chunk.size=5000)
	stopCluster(cl)
	all_results<-reformat_list_cor_res_to_df2(all_results)
	all_results<-all_results[which(all_results[,"is_better_than_null"]==TRUE),]
	#colnames(out_adj)<-c("edge_id", "gene_1", "gene_2","is_better_than_null", "cor_vals")
	return(all_results)
}










