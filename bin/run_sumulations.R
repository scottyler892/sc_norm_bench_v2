

#########################################################################
#########################################################################
#########################################################################
## SIMULATION CODE

###################################################################################
## <simulation_functions>




continue_func<-function(force, dest_clust_file){
    finished_clust_bool <- file.exists(dest_clust_file)
    print("#############################################")
    print(finished_clust_bool)
    print("#############################################")
    if (!(force | !finished_clust_bool)) {
        print("     already done & not forcing")
        return(FALSE)
    } else {
        return(TRUE)
    }
}



global_random_downsample_sim<-function(sim, name, out_dir, out_file, ds_percent, cell_subset=NULL, force = F){
    ##  select the pertinent cells
    if (!is.null(cell_subset)){
        #sim@assays[["colData"]]<-sim@assays[["colData"]][cell_subset,]
        scRNAseq_mat <- sim@assays[["counts"]][,cell_subset]
        #sim@assays[["counts"]]<-scRNAseq_mat
    } else {
        scRNAseq_mat <- sim@assays[["counts"]]
    }

    ## write the raw file
    raw_file<-paste(out_dir,paste(name,'_original.tsv',sep=""),sep='/')
    if (continue_func(force, raw_file)){
        write.table(scRNAseq_mat,sep='\t',col.names=NA,
            file=raw_file)
        system(paste("sed -i '1s/^/gene/' ",raw_file))
    }


    ## do the downsampling
    ds_file<-out_file#paste(out_dir,'expression_ds_norm_counts.tsv',sep='/')
    if (continue_func(force, ds_file)){    
        #original_dir <- getwd()
        #setwd(PROCESSING_DIR)
        ds_call<-c("python3 -m pyminer_norm.random_downsample_whole_dataset -i ",raw_file,' -o ', out_file,' -percent ',ds_percent)
        ds_call<-paste(ds_call,collapse=' ')
        print(ds_call)
        system(ds_call)
        #setwd(original_dir)
    }

    return()
}



downsample_norm_sim<-function(sim, out_dir, force = F){
    ##  normalize
    csum<-colSums(sim@assays[["counts"]])
    ds_cutoff <- as.integer(min(csum)*.95)
    scRNAseq_mat <- sim@assays[["counts"]]

    ## write the raw file
    raw_file<-paste(out_dir,'raw_expression.tsv',sep='/')
    if (continue_func(force, raw_file)){
        write.table(scRNAseq_mat,sep='\t',col.names=NA,
            file=raw_file)
    }

    ## do the downsampling
    ds_file<-paste(out_dir,'expression_ds_norm_counts.tsv',sep='/')
    if (continue_func(force, ds_file)){
        ds_call<-c("python3 -m pyminer_norm.downsample -i ",raw_file,' -o ', ds_file,' -num_transcripts ',ds_cutoff)
        ds_call<-paste(ds_call,collapse=' ')
        print(ds_call)
        system(ds_call)
        #setwd(original_dir)
    }

    temp_df<-read.csv(ds_file,header=TRUE,sep='\t')
    temp_mat<-as.matrix(temp_df[,-1])
    rownames(temp_mat)<-temp_df[,1]

    print(class(sim@assays[["counts"]]))
    print(dim(sim@assays[["counts"]]))

    sim@assays[["counts"]]<-temp_mat

    return(sim)
}



write_dataset<-function(sim, out_dir_vect, force = F){
    # """This just writes a copy of the dataset to the directory that pyminer will work in"""
    ## calculate the log2 cpm
    scRNAseq_mat <- log2((sim@assays[["counts"]])+1)
    for (out_dir in out_dir_vect){
        if (continue_func(force, paste(out_dir,'expression.tsv',sep='/'))){
            write.table(scRNAseq_mat,sep='\t',col.names=NA,
                file=paste(out_dir,'expression.tsv',sep='/'))
        }
    }
}


############


write_ground_truth<-function(sim, num_groups, out_dir_vect, force = F){
    # """
    # This takes in the simulation from splatter and writes
    # the cell type groups in a pyminer compatible format in the appropriate directories
    # """
    cell_id_df <- as.data.frame(sim@colData)
    cell_names <- rownames(cell_id_df)
    if (num_groups == 1){
        cell_group <- as.character(rep(0,length(cell_names)))
    }
    else{
        cell_group <- cell_id_df$Group
        cell_group <- as.integer(gsub("Group", "", cell_group))
        cell_group <- cell_group - 1
        cell_group <- as.character(cell_group)
    }
    df_out<-data.frame(cell_names,cell_group)
    for (out_dir in out_dir_vect){
        if (continue_func(force, paste(out_dir,'ground_truth_annotations.tsv',sep='/'))){
            write.table(df_out, col.names=FALSE, row.names=FALSE, sep='\t', file=paste(out_dir,'ground_truth_annotations.tsv',sep='/'))
        }
    }
}


write_batch_table<-function(in_dir){
    d1_file<-paste(in_dir,"d1_original.tsv",sep="/")
    d2_file<-paste(in_dir,"d2_original.tsv",sep="/")
    d1_head<-t(read.table(file = d1_file,header = F,nrows = 1,sep='\t'))
    d2_head<-t(read.table(file = d2_file,header = F,nrows = 1,sep='\t'))
    d1_table<-cbind(d1_head,rep("batch_1",length(d1_head)))
    d2_table<-cbind(d2_head,rep("batch_2",length(d2_head)))
    d1_table[1,1]<-"cell"
    d1_table[1,2]<-"batch"
    d2_table<-d2_table[-1,]
    merged_batch<-rbind(d1_table,d2_table)
    write.table(merged_batch,file=paste(in_dir,"batch_table.tsv",sep='/'),sep='\t', col.names=F, row.names=F)
}

############

get_sim_dataset<-function(num_groups, starter_seed, num_genes, num_cells, cell_state = FALSE){
    group_prob_vect <- runif(num_groups)
    group_prob_vect <- group_prob_vect + min(group_prob_vect) +1
    group_prob_vect <- group_prob_vect / sum(group_prob_vect)

    ## get the simulated dataset
    #starter_seed <- starter_seed+i
    params <- newSplatParams()
    print(getSlots(class(params)))
    params <- setParam(params, "seed", starter_seed)
    params <- setParam(params, "nGenes", num_genes)
    params <- setParam(params, "batchCells", num_cells)
    if (cell_state){
        params <- setParam(params, "de.downProb", 0)## probability of cell type specific loss of expression.
        params <- setParam(params, "de.prob", 1)## just controlling the fraction of genes that are differentially expressed
    } else {
        params <- setParam(params, "de.downProb", .25)## probability of cell type specific loss of expression.
        params <- setParam(params, "de.prob", .75)## just controlling the fraction of genes that are differentially expressed
    }
    params <- setParam(params, "de.facScale", .75)## how differentially expressed are the genes
    params <- setParam(params, "bcv.common", .75)## controlling the amount of dispersion 
    print(getSlots(class(params)))
    print(params)
    
    sim <- splatSimulate(params,method = "groups", 
                         group.prob = group_prob_vect)
    return(sim)
}


get_odd<-function(idx_vect){
    keep_bool<-idx_vect %% 2 == 1
    return(idx_vect[keep_bool])
}


get_even<-function(idx_vect){
    keep_bool<-idx_vect %% 2 == 0
    return(idx_vect[keep_bool])
}


filter_from_other_vect<-function(full_vect, rm_vect){
    keep_bool <- full_vect %in% rm_vect == FALSE
    return(full_vect[keep_bool])
}


allocate_cells<-function(cells,group_vect,data_details){
    shared_groups <- paste("Group",data_details[["shared_groups"]],sep="")
    d1_groups <- paste("Group",data_details[["d1_only"]],sep="")
    d2_groups <- paste("Group",data_details[["d2_only"]],sep="")
    
    d1_vect<-c()
    d2_vect<-c()
    for (i in 1:length(cells)){
        if (group_vect[i] %in% shared_groups) {
            ## randomly pick which group to add it to
            decider <- rnorm(1)
            #print(decider)
            if (decider > 0){
                d1_vect<-c(d1_vect,cells[i])
            } else {
                d2_vect<-c(d2_vect,cells[i])
            }
        }

        if (group_vect[i] %in% d1_groups){
            d1_vect<-c(d1_vect,cells[i])
        } 

        if (group_vect[i] %in% d2_groups){
            d2_vect<-c(d2_vect,cells[i])
        }
    }
    cell_list<-list()
    cell_list[["d1_cells"]]<-d1_vect
    cell_list[["d2_cells"]]<-d2_vect
    return(cell_list)
}


'%!in%' <- function(x,y)!('%in%'(x,y))


add_states<-function(sim, num_states, groups_to_add_to, percent_state, starter_seed, cell_name_vect = c()){
    print("simulating state")
    ## we'll start out adding only one state for simplicity
    if (length(cell_name_vect)==0){
        groups_to_add_to<-paste("Group",groups_to_add_to,sep="")
        print(paste(c("adding to:",groups_to_add_to),collapse=" "))
        current_cells<-as.character(sim@colData[which(sim@colData[,"Group"] %in% groups_to_add_to),"Cell"])
    } else {
        current_cells <- cell_name_vect
    }
    other_cells<-as.character(sim@colData[which(sim@colData[,"Cell"] %!in% current_cells),"Cell"])
    ## generate the weighting vectors
    state_vect <- rep(0,NUM_GENES)
    vect_of_genes_to_add_state_to<-1:as.integer(NUM_GENES * percent_state)
    state_vect[vect_of_genes_to_add_state_to] <- (rnorm(length(vect_of_genes_to_add_state_to))/50)+.75
    non_state_vect<- rep(0,NUM_GENES)
    vect_of_genes_to_add_non_state_to<-1:as.integer(NUM_GENES * percent_state)
    non_state_vect[vect_of_genes_to_add_non_state_to] <- abs((rnorm(length(vect_of_genes_to_add_non_state_to))/30))
    ## we will simulate each state independently, so that we can 
    #print(sim@colData[current_cells,])
    state_sim <- get_sim_dataset(1,starter_seed, NUM_GENES, NUM_CELLS, cell_state = TRUE)#length(current_cells))
    ## match up each original cell-type level "cell" with a new state-space "cell"
    for (i in 1:length(current_cells)){
        temp_state_vect <- as.numeric(state_sim[,i]@assays[["counts"]])
        cur_cell_str<-current_cells[i]
        cur_cell_idx<-which(as.character(sim@colData[,"Cell"])==cur_cell_str)
        #print(paste("adding state to:",cur_cell_str,cur_cell_idx))
        temp_orig_cell <- as.numeric(sim[,cur_cell_idx]@assays[["counts"]])
        #print(temp_orig_cell)
        #print(temp_state_vect)
        temp_mult <- (rnorm(1)/50)+.5
        #sim@assays[["counts"]][,cur_cell_idx]<-as.integer((temp_orig_cell*(1-percent_state))+(temp_state_vect*percent_state))
        new_type_weight <-temp_orig_cell*(1-(state_vect*temp_mult))
        new_state_weight <- temp_state_vect*(state_vect*temp_mult)
        #print(state_vect)
        #print(sum((new_type_weight-new_state_weight)**2))
        new_vect<-as.integer(new_type_weight+new_state_weight)
        sim@assays[["counts"]][,cur_cell_idx]<-new_vect
        #print(sum((new_vect - temp_orig_cell)**2))
    } 
    ## add a residual amount of the cell state to the remaining cells
    for (i in 1:length(other_cells)){
        temp_state_vect <- as.numeric(state_sim[,i+length(current_cells)]@assays[["counts"]])
        cur_cell_str<-other_cells[i]
        cur_cell_idx<-which(as.character(sim@colData[,"Cell"])==cur_cell_str)
        #print(paste("not adding state to:",cur_cell_str,cur_cell_idx))
        temp_orig_cell <- as.numeric(sim[,cur_cell_idx]@assays[["counts"]])
        #print(temp_orig_cell)
        #print(temp_state_vect)
        temp_mult <- abs((rnorm(1)/30))
        new_vect<-as.integer((temp_orig_cell*(1-(non_state_vect*temp_mult)))+(temp_state_vect*(non_state_vect*temp_mult)))
        sim@assays[["counts"]][,cur_cell_idx]<-new_vect
    }
    return(sim)
}


write_sim_datasets<-function(sim_dir,
                             num_sim_per_group_size,
                             shared_group_num,
                             total_num_group_vect,
                             starter_seed,
                             d1_ds = 0.3, 
                             d2_ds = 0.1,
                             do_state_mixture = FALSE,
                             num_states = 1,
                             state_in_shared = FALSE,
                             percent_state = 1/3,
                             state_in_both = FALSE,
                             b1_only = FALSE,
                             b1_all=FALSE,
                             force=FALSE){
    dir.create(sim_dir, showWarnings=FALSE)
    leader<-"inputs"
    full_path<-paste()
    sim_dir<-paste(sim_dir,"inputs",sep="/")
    dir.create(sim_dir, showWarnings=FALSE)
    ## generate the 'list_of_datasets_to_combine' list that contains:
    # level 1) scenario name
    # level 2) "DATASET_LIST" & "SHORT_NAMES" where dataset_list are the paths downstream of dataset_dir
    #example: list("DATASET_LIST" = ALL_DATASET_LIST[c(1,2)],  "SHORT_NAMES" = ALL_SHORT_NAMES[c(1,2)])
    list_of_datasets_to_combine <- list()
    ## if we have different 
    for(total_groups in total_num_group_vect){
        print(paste("doing",total_groups))
        group_name<-paste(total_groups, "_groups",sep="")
        leader_group <- paste(leader,group_name,sep="/")
        group_num_dir<-paste(sim_dir,group_name,sep="/")
        dir.create(group_num_dir, showWarnings=FALSE)
        ## haven't decided to do this yet
        for (i in 1:num_sim_per_group_size){
            ## set up the iteration
            iter_name <- paste("iter_",i,sep="")
            leader_iter<-paste(leader_group,iter_name,sep='/')
            iter_dir<-paste(c(group_num_dir,"/",iter_name),collapse = "")
            print(paste("iter_dir",iter_dir))
            ## first make the output directory for this iteratio
            dir.create(iter_dir, showWarnings=FALSE)
            ## normalize the simulation through UMI downsampling
            starter_seed<-starter_seed+i
            master_sim<-get_sim_dataset(total_groups,starter_seed, NUM_GENES, NUM_CELLS)

            ## now we'll go through and segregate the cell types by batch progressively
            for (j in shared_group_num){
                ## log the full dataset_list and short_names 
                clust_share_name<-paste(as.character(j),"_shared_clusters",sep="")
                leader_run <- paste(leader_iter, clust_share_name, sep="/")
                run_dir<-paste(iter_dir,clust_share_name,sep="/")
                dir.create(run_dir, showWarnings=FALSE)
                dataset_1 <- paste(run_dir,"dataset_1.tsv",sep="/")
                dataset_1_relative<-paste(leader_run, "dataset_1.tsv", sep="/")
                dataset_2 <- paste(run_dir,"dataset_2.tsv",sep="/")
                dataset_2_relative<-paste(leader_run, "dataset_2.tsv", sep="/")
                run_name<-gsub("/","_",leader_run)
                list_of_datasets_to_combine[[run_name]]<-list()
                list_of_datasets_to_combine[[run_name]][["iter"]]<-iter_name
                list_of_datasets_to_combine[[run_name]][["total_groups"]]<-total_groups
                list_of_datasets_to_combine[[run_name]][["num_shared"]]<-j
                list_of_datasets_to_combine[[run_name]][["d1_file"]]<-dataset_1
                list_of_datasets_to_combine[[run_name]][["d2_file"]]<-dataset_2
                list_of_datasets_to_combine[[run_name]][["DATASET_LIST"]]<-c(dataset_1_relative, dataset_2_relative)
                list_of_datasets_to_combine[[run_name]][["SHORT_NAMES"]]<-c("dataset_1", "dataset_2")
                all_group_vect<-1:total_groups
                if (j == 0){
                    list_of_datasets_to_combine[[run_name]][["shared_groups"]]<-NULL
                    list_of_datasets_to_combine[[run_name]][["not_shared_groups"]]<-all_group_vect
                    list_of_datasets_to_combine[[run_name]][["d1_only"]]<-get_odd(all_group_vect)
                    list_of_datasets_to_combine[[run_name]][["d2_only"]]<-get_even(all_group_vect)
                } else {
                    list_of_datasets_to_combine[[run_name]][["shared_groups"]]<-1:j
                    not_shared_groups<-filter_from_other_vect(all_group_vect,1:j)
                    list_of_datasets_to_combine[[run_name]][["d1_only"]]<-get_odd(not_shared_groups)
                    list_of_datasets_to_combine[[run_name]][["d2_only"]]<-get_even(not_shared_groups)
                    list_of_datasets_to_combine[[run_name]][["not_shared_groups"]]<-c(list_of_datasets_to_combine[[run_name]][["d1_only_groups"]],
                                                                                      list_of_datasets_to_combine[[run_name]][["d2_only_groups"]])
                }
                ## figure out which cells are going where
                # allocate the cells to their batches
                all_cells<-as.character(master_sim@colData[,"Cell"])
                all_groups<-as.character(master_sim@colData[,"Group"])
                cell_list<-allocate_cells(all_cells,all_groups, list_of_datasets_to_combine[[run_name]])
                ## add the states if we need to do that
                master_sim_group_vect<-as.character(master_sim@colData[,"Group"])
                master_sim_cell_vect<-as.character(master_sim@colData[,"Cell"])
                if (do_state_mixture){
                    print("entered state mixture")
                    if (b1_all){#(b1_only){
                        print("didn't enter main")
                        cells_to_add_to<-cell_list[["d1_cells"]]
                        groups_to_add_to<-NULL
                        sim<-add_states(master_sim, num_states, groups_to_add_to, percent_state, starter_seed, cell_name_vect = cells_to_add_to)
                    } else {
                        print("entered main")
                        added_state_yet<-FALSE
                        if (state_in_shared & j>0){# j is the number of shared groups
                            ## if we're adding it to shared groups, just add it to the first shared group
                            groups_to_add_to<-list_of_datasets_to_combine[[run_name]][["shared_groups"]][1]
                            groups_to_add_to<-paste("Group",groups_to_add_to,sep='')
                            cells_to_add_to<-master_sim_cell_vect[which(master_sim_group_vect %in% as.character(groups_to_add_to))]
                            print("1")
                            print(cells_to_add_to)
                            added_state_yet<-TRUE
                            #sim<-add_states(master_sim, num_states, groups_to_add_to, percent_state, starter_seed)
                        } else if (!state_in_shared & j<total_groups){
                            ## if we're adding it to not shared groups, we'll just add it to the first one
                            ## from each dataset that isn't shared
                            groups_to_add_to<-c(list_of_datasets_to_combine[[run_name]][["d1_only"]][1],
                                                list_of_datasets_to_combine[[run_name]][["d2_only"]][1])
                            groups_to_add_to<-paste("Group",groups_to_add_to,sep='')
                            cells_to_add_to<-master_sim_cell_vect[which(master_sim_group_vect %in% as.character(groups_to_add_to))]
                            print("2")
                            print(master_sim_group_vect)
                            print(groups_to_add_to)
                            which(master_sim_group_vect %in% groups_to_add_to)
                            print(cells_to_add_to)
                            added_state_yet<-TRUE
                            #sim<-add_states(master_sim, num_states, groups_to_add_to, percent_state, starter_seed)
                        }
                        if (state_in_both) {
                            groups_to_add_to<-c(list_of_datasets_to_combine[[run_name]][["shared_groups"]][1],
                                                list_of_datasets_to_combine[[run_name]][["d1_only"]][1],
                                                list_of_datasets_to_combine[[run_name]][["d2_only"]][1])
                            groups_to_add_to<-paste("Group",groups_to_add_to,sep='')
                            cells_to_add_to<-master_sim_cell_vect[which(master_sim_group_vect %in% as.character(groups_to_add_to))]
                            print("3")
                            print(cells_to_add_to)
                            added_state_yet<-TRUE
                            #sim<-add_states(master_sim, num_states, groups_to_add_to, percent_state, starter_seed)
                        }
                        if (added_state_yet==FALSE) {
                            print("4")
                            groups_to_add_to<-NULL
                            cells_to_add_to<-c()
                            print(cells_to_add_to)
                            sim <- master_sim
                        }
                    }
                } else {
                    cells_to_add_to <- c()
                    sim <- master_sim
                }
                if (b1_only & do_state_mixture){
                    ## filter cells to only those in batch 1
                    original_cells_to_add_to<-cells_to_add_to
                    cells_to_add_to<-as.character(cells_to_add_to[which(as.character(cells_to_add_to) %in% as.character(cell_list[["d1_cells"]]))])
                    print("subsetting for only batch 1")
                    print(cells_to_add_to %!in% cells_to_add_to)
                }
                if (do_state_mixture){
                    sim<-add_states(master_sim, num_states, groups_to_add_to, percent_state, starter_seed, cell_name_vect = cells_to_add_to)
                } else {
                    sim <- master_sim
                }
                ## go through all of the cells and change the  group table to update reflecting combination of group and state
                sim@colData[,"Group"]<-as.character(sim@colData[,"Group"])
                if (do_state_mixture){
                    for (i in 1:length(sim@colData[,"Cell"])){
                        if (as.character(sim@colData[i,"Cell"]) %in% cells_to_add_to){
                            sim@colData[i,"Group"]<-paste(as.character(sim@colData[i,"Group"]),"_state",sep='')
                            #print("TRUE")
                        } else {
                            #print("FALSE")
                        }
                    }
                }
                ## write the annotation file for the cell clusters
                cell_clust_table_file <- paste(run_dir,"cell_clusters.tsv",sep='/')
                list_of_datasets_to_combine[[run_name]][["cluster_file"]]<-cell_clust_table_file
                if (continue_func(force, cell_clust_table_file)){
                    cell_clust_table<-sim@colData[,c("Cell","Group")]
                    write.table(cell_clust_table,file=cell_clust_table_file,sep="\t",row.names=FALSE)
                }

                
                list_of_datasets_to_combine[[run_name]][["d1_cells"]]<-cell_list[["d1_cells"]]
                list_of_datasets_to_combine[[run_name]][["d2_cells"]]<-cell_list[["d2_cells"]]
                list_of_datasets_to_combine[[run_name]][["group_table"]]<-sim@colData[,c("Cell","Group")]
                allocated<-c(cell_list[["d1_cells"]],cell_list[["d2_cells"]])
                list_of_datasets_to_combine[[run_name]][["allocated"]]<-allocated
                list_of_datasets_to_combine[[run_name]][["not_allocated"]]<- all_cells %!in% allocated
                print("total cells:")
                print(length(cell_list[["d1_cells"]])+length(cell_list[["d2_cells"]]))

                ## now that we've set all of that up, we'll go through the simulation & 
                global_random_downsample_sim(sim, 
                                             "d1", 
                                             run_dir, 
                                             list_of_datasets_to_combine[[run_name]][["d1_file"]], 
                                             d1_ds, 
                                             cell_subset=cell_list[["d1_cells"]], 
                                             force = F)
                global_random_downsample_sim(sim, 
                                             "d2", 
                                             run_dir, 
                                             list_of_datasets_to_combine[[run_name]][["d2_file"]], 
                                             d2_ds, 
                                             cell_subset=cell_list[["d2_cells"]], 
                                             force = F)

                print("writing batch table")
                write_batch_table(run_dir)
            }
        }
    }
    return(list_of_datasets_to_combine)
}



## </simulation_functions>
###########################################################################################
