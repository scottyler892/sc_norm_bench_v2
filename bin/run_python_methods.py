#!/usr/local/env python3
import os
import scvi
import scanpy as sc
import argparse
import numpy as np
import random
import fileinput
import csv
import gzip
import os
import scipy.io
from pyminer.common_functions import process_dir, write_table, strip_split
#from pyminer.pyminer_tm_norm import run_tm_norm

def update_cell_names(adata, short_name_list):
	## by default, the read-in appends batch number to cell id concatenated with a '-'
	new_name_dict = {}
	for i in range(len(adata.obs.index)):
		temp_cell = adata.obs.index[i].split("-")
		primary_cell_name = "-".join(temp_cell[:-1])
		new_name = short_name_list[int(temp_cell[-1])]+"||"+primary_cell_name
		new_name_dict[adata.obs.index[i]]=new_name
	adata.obs.rename(index=new_name_dict, inplace=True)
	return(adata)


def update_single_dataset_names(adata,dset_name):
	new_name_dict = {}
	for i in range(len(adata.obs.index)):
		if "-" in adata.obs.index[i]:
			temp_cell = adata.obs.index[i].split("-")
			primary_cell_name = "-".join(temp_cell[:2])
		else:
			primary_cell_name = adata.obs.index[i]
		new_name = dset_name+"||"+primary_cell_name
		new_name_dict[adata.obs.index[i]]=new_name
	adata.obs.rename(index=new_name_dict, inplace=True)
	return(adata)


def load_gz(in_gz):
	feat_file = gzip.open(in_gz,'rb')
	feats = feat_file.read().decode().split('\n')
	feat_file.close()
	if feats[-1]=="":
		feats = feats[:-1]
	feature_ids = [strip_split(row)[0] for row in feats]
	return(feature_ids)



def update_original_names(adata, bcodes):
	## by default, the read-in appends batch number to cell id concatenated with a '-'
	print(len(adata.obs.index))
	print(len(bcodes))
	assert len(adata.obs.index) == len(bcodes)
	new_name_dict = {}
	for i in range(len(adata.obs.index)):
		new_name_dict[adata.obs.index[i]]=bcodes[i]
	adata.obs.rename(index=new_name_dict, inplace=True)
	return(adata)


def load_mtx(matrix_dir):
	mat = scipy.io.mmread(os.path.join(matrix_dir, "matrix.mtx.gz"))
	features_path = os.path.join(matrix_dir, "features.tsv.gz")
	feature_ids = load_gz(features_path)
	barcodes_path = os.path.join(matrix_dir, "barcodes.tsv.gz")
	barcodes = load_gz(barcodes_path)
	adata = sc.AnnData(mat)
	adata = update_original_names(adata, feature_ids)
	adata = adata.T
	adata = update_original_names(adata, barcodes)
	return(adata)


def load_tsv(in_file):
	out_data = scvi.data.read_csv(in_file,
				                      delimiter="\t").T
	return(out_data)


def get_all_dsets(in_file_list=[], short_name_list=[], mtx_dirs=[]):
	assert len(in_file_list+mtx_dirs)>1
	dataset_lengths=[]
	read_in_function = [load_tsv] * len(in_file_list) + [load_mtx] * len(mtx_dirs)
	combined_inputs = in_file_list + mtx_dirs
	print("loading: "+combined_inputs[0])
	out_data = read_in_function[0](combined_inputs[0])
	out_data = update_single_dataset_names(out_data, short_name_list[0])
	num_cells = len(out_data.obs.index)
	b_vect = [str(0) for blah in range(num_cells)]
	print("\t",num_cells, "cells")
	#dataset_lengths.append()
	print(out_data)
	individual_objs = []
	for i in range(1,len(combined_inputs)):
		temp_dset = combined_inputs[i]
		print("loading: "+temp_dset)
		temp_loaded = read_in_function[i](temp_dset)
		temp_loaded = update_single_dataset_names(temp_loaded, short_name_list[i])
		num_cells = len(temp_loaded.obs.index)
		print("this dset has:",num_cells,"cells")
		b_vect += [str(i) for blah in range(num_cells)]
		individual_objs.append(temp_loaded)
		#out_data = out_data.concatenate(temp_loaded)
	out_data = out_data.concatenate(*individual_objs)
	out_data.obs["batch"]["batch"]=b_vect## not sure why the batch wasn't loading correctly before..
	print(out_data.obs["batch"])
	return(out_data)


def process_and_get_latent(adata,
                    all_genes=False,
                    species="mmusculus"):
	#sc.set_figure_params(figsize=(4, 4))
	print(adata)
	#sys.exit()
	sc.pp.normalize_total(adata, target_sum=1e4)
	sc.pp.log1p(adata)
	adata.raw = adata # freeze the state in `.raw`
	if not all_genes:
		sc.pp.highly_variable_genes(
		     adata,
		     n_top_genes=2000,
		     subset=True,
		     layer="counts",
		     flavor="seurat_v3",
		     batch_key="batch"
		)
	else:
		print("skipping feature selection & doing it on the whole input")
	## do scanorama
	sc.tl.pca(adata)
	sc.external.pp.scanorama_integrate(adata, 'batch')
	## do scVI
	try:
		scvi.data.setup_anndata(adata, layer="counts", batch_key="batch")
	except:
		scvi.model.SCVI.setup_anndata(adata, layer="counts", batch_key="batch")
	vae = scvi.model.SCVI(adata, n_layers=2, n_latent=30)
	try:
		vae.train(use_gpu=True)
	except:
		print("\n\nGPU training failed, switching to cpu")
		vae.train(use_gpu=False)
	adata.obsm["X_scVI"] = vae.get_latent_representation()
	## do bbknn ## ended up not including bbknn in comparison because it's impossible
	## to compare exactly with only adjusting connectivities & distances
	## since the clustering would have to happen directly on this, so comparison wouldn't
	## really be 1-to-1 with the others
	# adata_copy = sc.external.pp.bbknn(adata, 'batch', copy=True)
	return(adata)


def adata_tm_norm(adata, species="mmusculus"):
	print(type(adata.layers["counts"]))
	print(adata.layers["counts"])
	print(adata.layers["counts"].shape)
	return (run_tm_norm(adata.layers["counts"].copy().T, 
                                        list(adata.var_names), 
                                        species=species))


def get_out_table(num_tab, cell_ids, genes=None):
	num_factors = num_tab.shape[1]
	if genes is None:
		factor_labels = ["factor_"+str(i) for i in range(num_factors)]
	else:
		factor_labels = genes
	num_tab = num_tab.T
	temp_out = np.concatenate((np.array([factor_labels]).T,num_tab),axis=1)
	temp_out = np.concatenate((np.array([["vars"]+cell_ids]),temp_out),axis=0)
	return(temp_out.tolist())


def final_clean_cell_names(cell_names):
	out_names = []
	for name in cell_names:
		temp_name = name.split("-")
		out_names.append(temp_name[0]+"."+temp_name[1])
	return(out_names)


def process_and_write_latent(adata, out_dir, original_names):
	out_dir = process_dir(out_dir)
	scanorama_dir = process_dir(os.path.join(out_dir,"scanorama"))
	scvi_dir = process_dir(os.path.join(out_dir,"sc_vi"))
	scan_out = get_out_table(adata.obsm["X_scanorama"], original_names)#final_clean_cell_names(list(adata.obs.index)))
	scVI_out = get_out_table(adata.obsm["X_scVI"], original_names)#final_clean_cell_names(list(adata.obs.index)))
	write_table(scan_out,os.path.join(scanorama_dir, "normalized_counts_log2.tsv"))
	write_table(scVI_out,os.path.join(scvi_dir, "normalized_counts_log2.tsv"))
	return()


def low_mem_writer(out_file, mat, rows, cols, sep="\t"):
	assert(len(cols)==mat.shape[1])
	assert(len(rows)==mat.shape[0])
	f=open(out_file, "w")
	## write the header
	print(out_file)
	print("writing header")
	f.write("var"+sep+sep.join(list(map(str,cols)))+"\n")
	for i in range(mat.shape[0]):
		#print(i)
		merged_line=sep.join(list(map(str,mat[i,:].tolist())))
		#print("writing")
		if i<(mat.shape[0]-1):
			f.write(rows[i]+sep+merged_line+"\n")
		else:
			## no new line
			f.write(rows[i]+sep+merged_line)
	f.close()
	return()


def process_and_write_tm(out_mat, genes, out_dir, original_names):
	tm_norm_dir = process_dir(os.path.join(out_dir,"tm_norm"))
	out_file = os.path.join(tm_norm_dir, "normalized_counts_log2.tsv")
	low_mem_writer(out_file, out_mat, genes, original_names)
	return()


def check_if_tm_norm_done(out_dir):
	bham_dir = out_dir+"/tm_norm"
	bham_out = os.path.join(bham_dir, "normalized_counts_log2.tsv")
	bham_out_gz = os.path.join(bham_dir, "normalized_counts_log2.tsv.gz")
	print("checking for "+bham_out)
	if (os.path.isfile(bham_out) or os.path.isfile(bham_out_gz)):
		print("already finished and not forcing")
		return(True)
	else:
		return(False)


def check_if_done(out_dir):
	print("looking in "+ out_dir)
	if out_dir is None:
		return(False)
	if out_dir is not None:
		if not os.path.isdir(out_dir):
			return(False)
	scan_dir = out_dir+"/scanorama"
	sc_vi_dir = out_dir+"/sc_vi"
	scan_out = os.path.join(scan_dir, "normalized_counts_log2.tsv")
	sc_vi_out = os.path.join(scan_dir, "normalized_counts_log2.tsv")
	scan_out_gz = os.path.join(scan_dir, "normalized_counts_log2.tsv.gz")
	sc_vi_out_gz = os.path.join(scan_dir, "normalized_counts_log2.tsv.gz")
	print("checking for "+scan_out+" and "+sc_vi_out)
	if (os.path.isfile(scan_out) and os.path.isfile(sc_vi_out)) or (os.path.isfile(scan_out_gz) and os.path.isfile(sc_vi_out_gz)):
		print("already finished and not forcing")
		return(True)
	else:
		return(False)


def get_original_cell_names(in_file_list, short_name_list, mtx_dirs=[]):
	out_names=[]
	for i in range(len(in_file_list)):
		in_file =  in_file_list[i]
		temp_short_name = short_name_list[i]
		first = True
		for line in fileinput.input(in_file):
			if first:
				temp_names = strip_split(line)[1:]
				first = False
			break
		fileinput.close()
		out_names += [temp_short_name+"||"+tn for tn in temp_names]
	for i in range(len(mtx_dirs)):
		if os.path.isfile(mtx_dirs[i]+"/barcodes.tsv.gz"):
			bcode_file = gzip.open(mtx_dirs[i]+"/barcodes.tsv.gz",'rb')
			bcodes = bcode_file.read()
			bcode_file.close()
			bcodes = bcodes.decode().split('\n')
		elif os.path.isfile(mtx_dirs[i]+"/barcodes.tsv"):
			bcode_file = open(mtx_dirs[i]+"/barcodes.tsv",'r')
			bcodes = bcode_file.read()
			bcode_file.close()
			bcodes = bcodes.split('\n')
		## hack in case there's a trailing \n at the end of the file
		if bcodes[-1]=="":
			bcodes = bcodes[:-1]
		temp_short_name = short_name_list[i+len(in_file_list)]
		out_names += [temp_short_name+"||"+tn for tn in bcodes]
	return(out_names)



def run_sc_VI_main(in_file_list, short_name_list, do_plot=False, out_dir=None, force=False, mtx_dirs=[], all_genes=False, species="mmusculus"):
	## first check if we're not forcing
	scan_and_scvi_done=check_if_done(out_dir)
	tm_norm_done = True#check_if_tm_norm_done(out_dir)
	if force or (not scan_and_scvi_done or not tm_norm_done):
		original_names = get_original_cell_names(in_file_list, short_name_list, mtx_dirs=mtx_dirs)
		adata = get_all_dsets(in_file_list, short_name_list, mtx_dirs=mtx_dirs)
		adata.layers["counts"] = adata.X.copy() # preserve counts
		print(adata)
		if force or not scan_and_scvi_done:
			adata = process_and_get_latent(adata, all_genes=all_genes)
			if out_dir is not None:
				process_and_write_latent(adata, out_dir, original_names)
		if do_plot:
			sc.pp.neighbors(adata, use_rep="X_scanorama")
			sc.tl.leiden(adata)
			sc.tl.umap(adata)
			sc.pl.umap(
			    adata,
			    color=["batch", "leiden"],
			    frameon=False,
			    ncols=1,
			)
			sc.pp.neighbors(adata, use_rep="X_scVI")
			sc.tl.leiden(adata)
			sc.tl.umap(adata)
			sc.pl.umap(
			    adata,
			    color=["batch", "leiden"],
			    frameon=False,
			    ncols=1,
			)
		####################################
		if False:#force or not tm_norm_done:
			tm_norm_mat = adata_tm_norm(adata, species)
			#print("adding to adata")
			#adata.layers["tm_norm"]=tm_norm_mat.T
			print("writing")
			process_and_write_tm(tm_norm_mat, list(adata.var_names), out_dir, original_names)
		return(adata)
	else:
		return()



if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument("-infile_paths", '-i',
	    help="the input datasets to be combined in tsv format",
	    nargs="+",
	    default=[])

	parser.add_argument("-mtx_dirs", "-mtx", '-m',
	    help="the input datasets to be combined in mtx directory format",
	    nargs="+",
	    default=[])

	parser.add_argument("-short_names", '-sn','-names',
	    help="short nicknames for the input datasets (in the same order)",
	    nargs="+")

	parser.add_argument("-species", 
	    help="species argument in the format of gProfiler (eg: mmusculus, hsapiens)",
	    default="mmusculus")

	parser.add_argument("-out_dir", '-out','-o',
	    help="The top out-dir, the script will automatically make sub-dirs for each method")

	parser.add_argument("-force","-f", 
	    help="if we want to force it to re-do the correction instead of not over-writing old results. Default=False",
	    action="store_true")

	parser.add_argument("-all_genes",
	    help="don't do hvg feature selection, but instead do the analysis on all of the genes in this dataset.",
	    action="store_true")

	parser.add_argument("-rand_seed", 
	    help="the random seed to use. default=123456",
	    default=123456)

	args = parser.parse_args()

	np.random.seed(args.rand_seed)
	random.seed(args.rand_seed)

	run_sc_VI_main(in_file_list=args.infile_paths, 
		           mtx_dirs = args.mtx_dirs,
		           short_name_list=args.short_names, 
		           do_plot=False, 
		           out_dir=args.out_dir, 
		           force=args.force,
		           all_genes=args.all_genes,
		           species=args.species)


	########################
	# in_file_list=["/home/scott/bin/sc_norm_bench_v2/data/biologic_datasets/in_data/intestine_normal/intestine_norm.tsv","/home/scott/bin/sc_norm_bench_v2/data/biologic_datasets/in_data/intestine_irradiated/intestine_irradiated.tsv"]
	# intest=run_sc_VI_main(in_file_list=in_file_list, short_name_list=["homeostatic_intestine","irradiated_intestine"], out_dir = '/home/scott/Downloads/DUMMY/dummy_intest/')

	# zero_shared=["/home/scott/bin/sc_norm_bench/data/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/0_shared_clusters/dataset_1.tsv","/home/scott/bin/sc_norm_bench/data/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/0_shared_clusters/dataset_2.tsv"]
	# zero_syn = run_sc_VI_main(in_file_list=zero_shared, short_name_list=["b1","b2"])

	# all_shared=["/home/scott/bin/sc_norm_bench/data/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/6_shared_clusters/dataset_1.tsv","/home/scott/bin/sc_norm_bench/data/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/6_shared_clusters/dataset_2.tsv"]	
	# all_syn = run_sc_VI_main(in_file_list=all_shared, short_name_list=["b1","b2"])

	# orthogonal = ["/home/scott/bin/sc_norm_bench/data/biologic_datasets/in_data/neuron_1k/neuron_1k_v3.tsv", "/home/scott/bin/sc_norm_bench/data/biologic_datasets/in_data/intestine_normal/intestine_norm.tsv"]
	# brain_int = run_sc_VI_main(in_file_list=orthogonal, short_name_list=["brain_1k_v3","homeostatic_intestine"])

	# state_in_all_files = ["/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/6_shared_clusters/dataset_1.tsv", "/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/6_shared_clusters/dataset_2.tsv"]
	# state_in_all = run_sc_VI_main(in_file_list=state_in_all_files, short_name_list=["dataset_1","dataset_2"])

	# state_in_4_files = ["/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/4_shared_clusters/dataset_1.tsv", "/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_both_TRUE_b1_all/inputs/6_groups/iter_3/4_shared_clusters/dataset_2.tsv"]
	# state_in_4 = run_sc_VI_main(in_file_list=state_in_4_files, short_name_list=["dataset_1","dataset_2"])


	# state_in_different_type_files = ["/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_shared_TRUE_b1_only_FALSE/inputs/6_groups/iter_3/0_shared_clusters/dataset_1.tsv","/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_shared_TRUE_b1_only_FALSE/inputs/6_groups/iter_3/0_shared_clusters/dataset_2.tsv"]
	# state_in_different_type = run_sc_VI_main(in_file_list=state_in_different_type_files, short_name_list=["dataset_1","dataset_2"])

	# state_in_different_types_2_files = ["/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_shared_FALSE_b1_only_FALSE/inputs/6_groups/iter_3/4_shared_clusters/dataset_1.tsv","/home/scott/Documents/scRNAseq/norm_comparison/simulations/with_states_TRUE_state_in_shared_FALSE_b1_only_FALSE/inputs/6_groups/iter_3/4_shared_clusters/dataset_2.tsv"]
	# state_in_different_types_2 = run_sc_VI_main(in_file_list=state_in_different_types_2_files, short_name_list=["dataset_1","dataset_2"])


## python3 bin//run_python_methods.py  -i  data/biologic_datasets/in_data//intestine_norm/intestine_norm_0.tsv data/biologic_datasets/in_data//neuron_1k_v3/neuron_1k_v3_0.tsv data/biologic_datasets/in_data//neuron_1k_v3_50pcnt_ds/neuron_1k_v3_50pcnt_ds_0.tsv  -sn  intestine_norm_0 neuron_1k_v3_0 neuron_1k_v3_50pcnt_ds_0  -out_dir  data/biologic_datasets/output//cross_val/same_run_downsampled_with_orthogonal_1
