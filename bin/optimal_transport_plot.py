import os
import ot
import ot.plot
import random
import numpy as np
import pandas as pd
from copy import deepcopy
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter
from sklearn.metrics.pairwise import euclidean_distances as euc
#import toyplot as plt


######################################
def get_all_ots(list_of_matrices):
    out_mats = [list_of_matrices[0]]
    for i in range(len(list_of_matrices)-1):
        out_mats.append(get_single_ot_pair(list_of_matrices[i],list_of_matrices[i+1]))
    out_mats[0] = get_single_ot_pair(list_of_matrices[1],list_of_matrices[0])
    return(out_mats)


def get_single_ot_pair_order(xs, xt):
    assert xs.shape[0]==xt.shape[0]
    n=xs.shape[0]
    # loss matrix
    a, b = np.ones((n,)) / n, np.ones((n,)) / n  # uniform distribution on samples
    M = ot.dist(xs, xt)
    G0 = ot.emd(a, b, M)
    ## now we take that matching matrix and 
    new_idx_order = np.zeros(xt.shape[0],dtype=int)
    for i in range(xt.shape[0]):
        new_idx_order[i]=np.where(G0[i,:]>0)[0][0]
    return(new_idx_order)


def get_single_ot_pair(xs, xt):
    assert xs.shape[0]==xt.shape[0]
    new_idx_order=get_single_ot_pair_order(xs, xt)
    xt=xt[new_idx_order,:]
    return(xt)

#######################################


def get_theta(source_x, source_y, target_x, target_y):
    return(math.atan((target_y-source_y)/(target_x-source_x)))


def rotate(x0,y0,theta):
    ## theta is between -.5 and +0.5
    x1=x0*np.cos(theta) - y0*np.sin(theta)
    y1=x0*np.sin(theta) + y0*np.cos(theta)
    return(x1, y1)

def min_max_norm(in_vect,post_shrink = 1):
    in_vect -= np.min(in_vect)
    in_vect /= np.max(in_vect)
    in_vect /= post_shrink
    return(in_vect)


def add_s(base_cells, bottom_theta):
    bottom_cell_idxs = np.where(base_cells[:,1]<0.5)[0]
    base_cells[bottom_cell_idxs,1], base_cells[bottom_cell_idxs,0] = rotate(base_cells[bottom_cell_idxs,1],
                                            base_cells[bottom_cell_idxs,0],
                                            bottom_theta)
    base_cells[bottom_cell_idxs,1] = min_max_norm(base_cells[bottom_cell_idxs,1],post_shrink=7)
    base_cells[bottom_cell_idxs,1] -= bottom_theta
    base_cells[bottom_cell_idxs,1] += .075
    # ## shrink the in-group variance a bit
    # y[in_bottom] += bottom_theta ## offset them as well
    # print("post theta:", min(y[in_bottom]),max(y[in_bottom]))
    # y[in_bottom] += .25
    # print("post offset:",min(y[in_bottom]),max(y[in_bottom]))
    return(base_cells)


def make_single_dataset(n_cells_per_sample,bottom_theta):
    y = np.random.uniform(-1,1,n_cells_per_sample)+np.random.normal(0,.2,n_cells_per_sample)
    x = np.random.normal(0,.032,n_cells_per_sample)
    y_orig = deepcopy(y)
    x_orig = deepcopy(x)
    #######
    ## make the top arms
    in_top = y>0
    in_top_left = in_top * (np.random.uniform(-1,1,n_cells_per_sample)>0)
    in_top_right = in_top * (in_top.astype(int) - in_top_left.astype(int))
    in_top_right = in_top_right.astype(bool)
    in_bottom = y<=0
    y[in_top_left], x[in_top_left]=rotate(y[in_top_left], x[in_top_left], -0.15)
    y[in_top_right], x[in_top_right]=rotate(y[in_top_right], x[in_top_right], 0.15)
    ## offset them slightly
    group_separator=0.3
    y[in_top_left]+=group_separator
    x[in_top_left]-=group_separator
    y[in_top_right]+=group_separator
    x[in_top_right]+=group_separator
    y[in_bottom]-=group_separator
    ## before we rotate the bottom, copy it for cell type only signal
    out_b_cell_type_only = np.zeros((n_cells_per_sample,2))
    out_b_cell_type_only[:,0] = min_max_norm(deepcopy(x))
    out_b_cell_type_only[:,1] = min_max_norm(deepcopy(y)+np.random.normal(0,.03,n_cells_per_sample))
    ## now add the cell state
    y[in_bottom], x[in_bottom]=rotate(y[in_bottom], x[in_bottom], bottom_theta)
    ## generate an arbitray null
    out_null = np.zeros((n_cells_per_sample,2))
    out_null[:,0] = x_orig
    out_null[:,1] = y_orig
    out_b = np.zeros((n_cells_per_sample,2))
    ## normalize them
    # Add a small amount of noise 
    out_b[:,0]=min_max_norm(x)
    out_b[:,1]=min_max_norm(deepcopy(y)+np.random.normal(0,.03,n_cells_per_sample))
    print(min(out_b[in_bottom,1]),max(out_b[in_bottom,1]))
    out_null = get_single_ot_pair(out_b_cell_type_only,out_null)
    #out_b = get_single_ot_pair(out_b_cell_type_only,out_b)
    out_b = add_s(deepcopy(out_b_cell_type_only), bottom_theta)
    #new_idx_order = np.argsort(y)
    return(
        out_null, 
        out_b_cell_type_only,
        out_b,
        in_bottom)


def make_data(n_cells_per_sample,
              sample_bottom_theta=[-.25,-.15,-.13,-.1,0,.03,.13,.2]):
    all_null = []
    all_b = []
    all_bs = []
    for theta in sample_bottom_theta:
        temp_null, temp_b, temp_bs, temp_bottom = make_single_dataset(n_cells_per_sample,theta)
        all_null.append(temp_null)
        all_b.append(temp_b)
        all_bs.append(temp_bs)
        print("mk data min max:",np.min(temp_bs[temp_bottom,1]),np.max(temp_bs[temp_bottom,1]))
        plt.scatter(temp_bs[:,0],temp_bs[:,1])
        plt.show()
    if True:
        ## reorder the cells by the middle of all_b
        all_b_final = []
        all_bs_final = []
        ref_idx = int(len(sample_bottom_theta)/2)
        for i in range(len(sample_bottom_theta)):
            ## TODO: 
            new_order = get_single_ot_pair_order(all_b[ref_idx], all_b[i])
            all_b_final.append(all_b[i][new_order,:])
            all_bs_final.append(all_bs[i][new_order,:])
        if False:
            all_data = get_all_ots(all_null+all_b_final+all_bs_final)
            null = all_data[:len(all_null)]
            b = all_data[len(all_null):int(len(all_null)*2)]
            bs = all_data[int(len(all_null)*2):]
        else:
            all_data = get_all_ots(all_null+all_b_final)
            null = all_data[:len(all_null)]
            b = all_data[len(all_null):]
            bs = []
            for i in range(len(b)):
                bs.append(add_s(deepcopy(b[i]),sample_bottom_theta[i]))
    else:
        null = all_null
        b = all_b
        bs = all_bs
    # new_order = get_single_ot_pair_order(b[1], b[0])
    # null[0]=null[0][new_order,:]
    # b[0]=b[0][new_order,:]
    # bs[0]=bs[0][new_order,:]
    in_bottom = []
    for i in range(len(b)):
        in_bottom.append(np.where(b[i][:,1]<0.5)[0])
    return(null, b, bs, in_bottom)


def create_plots_within_stage(local_data, sample_bottom_theta=[], colors=None, out_frame="",
                              xmin=-.2,xmax=1.2,ymin=-.2, ymax=1.2):
    ####################
    ## set up the output plots
    n_samples = len(local_data)
    fig = plt.figure()
    gs = gridspec.GridSpec(n_samples, 
                           n_samples+1)
    ax = plt.subplot(gs[:,:-1])
    ax.tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)
    individual_ax=[]
    for i in range(n_samples):
        individual_ax.append(plt.subplot(gs[i,-1]))
        individual_ax[-1].tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)
    ####################
    ## get the colors
    if colors is None:
        cmap = cm.cool
        sample_bottom_theta_norm = np.array(sample_bottom_theta)
        sample_bottom_theta_norm -= np.min(sample_bottom_theta_norm)
        sample_bottom_theta_norm /= np.max(sample_bottom_theta_norm)
        colors = [cmap(sample_bottom_theta_norm[i]) for i in range(sample_bottom_theta_norm.shape[0])]
    else:
        pass
    #####################
    #sample_bottom_theta_norm *= 256
    #sample_colors = cmap(sample_bottom_theta)
    for i in range(len(local_data)):
        print()
        ax.scatter(local_data[i][:,0],
                    local_data[i][:,1],
                    s=3,
                    c=[colors[i]]*local_data[i].shape[0])
        individual_ax[i].scatter(local_data[i][:,0],
                    local_data[i][:,1],
                    s=1,
                    c=[colors[i]]*local_data[i].shape[0])
        individual_ax[i].set_xlim(xmin,xmax)
        individual_ax[i].set_ylim(ymin,ymax)
    ax.set_xlim(xmin,xmax)
    ax.set_ylim(ymin,ymax)
    if out_frame == "":
        plt.show()
    else:
        plt.savefig(out_frame, dpi=150)
    return()


def smooth_field(in_field, smoothness=7):
    blurred = gaussian_filter(in_field, sigma=smoothness)
    return(blurred)


def get_1d_field(n_bins, shrinking_factor, smoothness=7):
    field = (np.random.choice(a=[-1, 1], size=(n_bins**2), p=[.5, .5])*np.random.negative_binomial(.005,.001,n_bins**2))+np.random.uniform(0,1,n_bins**2)
    field = field.reshape((n_bins,n_bins))
    field = smooth_field(field, smoothness=7)
    ## min max and scale it
    field -= np.min(field)
    field /= (np.max(field)/shrinking_factor)
    ## now center it
    field -= np.mean(field)
    return(field)




def plot_warp_field(n_bins, field_x, field_y,leader="",out_dir=""):
    start_x = np.array(list(range(n_bins))*n_bins).reshape((n_bins,n_bins))/n_bins
    start_y = deepcopy(start_x).T
    flat_field_x = deepcopy(field_x).flatten()
    flat_field_y = deepcopy(field_y).flatten()
    # end_x = start_x+field_x
    # end_y = start_y+field_y
    start_x = start_x.flatten()
    # end_x = end_x.flatten()
    start_y = start_y.flatten()
    # end_y = end_y.flatten()
    for i in range(start_x.shape[0]):
        plt.arrow(start_x[i], start_y[i], flat_field_x[i], flat_field_y[i], color="k", head_width=0.008)
    plt.tick_params(left = False, right = False , labelleft = False ,
                labelbottom = False, bottom = False)
    if out_dir != "":
        if os.path.isdir(out_dir):
            out_file = os.path.join(out_dir,leader+str("_%016x" % random.getrandbits(128))+".png")
            plt.savefig(out_file, dpi=150)
    else:
        plt.show()


def get_random_field(n_bins = 40, shrinking_factor = .15, do_plot=False, smoothness=7):
    ## shrinking factor is what percent of the total range should the max deformation be
    field_x = get_1d_field(n_bins, shrinking_factor, smoothness=smoothness)
    field_y = get_1d_field(n_bins, shrinking_factor)
    if do_plot:
        plot_warp_field(n_bins, field_x, field_y)
    return(field_x, field_y)


def add_warp(local_data, x_deform, y_deform, start_x, start_y, temp_mag):
    start_x = start_x.flatten()
    start_y = start_y.flatten()
    start_positions = np.zeros((start_x.shape[0],2))
    x_deform = x_deform.flatten()
    y_deform = y_deform.flatten()
    start_positions[:,0]=start_x
    start_positions[:,1]=start_y
    deform_flat = np.zeros((start_x.shape[0],2))
    deform_flat[:,0]=x_deform
    deform_flat[:,1]=y_deform
    ## get the relative inverse distance
    local_warp_field = np.zeros(local_data.shape)
    dist = euc(local_data, start_positions)
    for i in range(dist.shape[0]):
        temp_dists = deepcopy(dist[i,:])
        # cutoff = temp_dists[np.argsort(temp_dists)[int(temp_dists.shape[0]*.01)]]
        # temp_dists[temp_dists<cutoff]=0
        # dist[i,:]=temp_dists
        local_warp_field[i,:]=(deform_flat[np.argsort(temp_dists)[0],:]*temp_mag)+np.random.normal(0,.01,2)
    for i in range(local_data.shape[0]):
        plt.arrow(local_data[i,0], local_data[i,1], local_warp_field[i,0], local_warp_field[i,1], color="k", head_width=0.008)
    plt.show()
    return(local_data+local_warp_field)


def apply_random_field(temp_data, 
                       n_bins = 40, 
                       shrinking_factor=0.15, 
                       same_field_to_all = True,
                       smoothness = 7,
                       leader="",
                       out_dir=""):
    start_x = np.array(list(range(n_bins))*n_bins).reshape((n_bins,n_bins))/n_bins
    start_y = deepcopy(start_x).T
    x_deform, y_deform = get_random_field(n_bins=n_bins, shrinking_factor=shrinking_factor, smoothness = smoothness)
    magnitude = np.random.normal(0,.25,len(temp_data))+2
    all_deform_x = []
    all_deform_y = []
    out_data = []
    for i in range(len(temp_data)):
        if not same_field_to_all:
            ## redefine the deformation topologies
            x_deform, y_deform = get_random_field(n_bins=n_bins, shrinking_factor=shrinking_factor, smoothness = smoothness)
        orig = deepcopy(temp_data[i])
        out_data.append(add_warp(deepcopy(temp_data[i]), 
                                deepcopy(x_deform), 
                                deepcopy(y_deform), 
                                start_x, 
                                start_y, 
                                magnitude[i]))
        print("sum sqr diffs:",np.sum((out_data[i]-orig)**2))
        all_deform_x.append(deepcopy(x_deform))
        all_deform_y.append(deepcopy(y_deform))
        if not same_field_to_all:
            plot_warp_field(n_bins, all_deform_x[i], all_deform_y[i],leader=leader+"_"+str(i),out_dir=out_dir)
    ## rescale all of the data
    plot_warp_field(n_bins, all_deform_x[-1], all_deform_y[-1],leader=leader,out_dir=out_dir)
    #create_plots_within_stage(deepcopy(out_data), sample_bottom_theta)
    return(out_data, x_deform, y_deform, magnitude)


def get_generic_alignment(observed_data):
    n_samples = len(observed_data)
    ref_sample_idx = int(deepcopy(n_samples)/2)
    target_locations = []#np.zeros(observed_data[0].shape)
    for i in range(n_samples):
        temp_new_loc = deepcopy(observed_data[ref_sample_idx])+np.random.normal(0,.04,observed_data[ref_sample_idx].shape)
        temp_new_loc = get_single_ot_pair(deepcopy(observed_data[i]),
                           deepcopy(temp_new_loc))
        #target_locations.append(deepcopy(observed_data[ref_sample_idx])+np.random.normal(0,.04,observed_data[ref_sample_idx].shape))
        target_locations.append(temp_new_loc)
    return(target_locations)


#################################################################
def get_colors(sample_bottom_theta, cmap = cm.cool):
    sample_bottom_theta_norm = np.array(sample_bottom_theta)
    sample_bottom_theta_norm -= np.min(sample_bottom_theta_norm)
    sample_bottom_theta_norm /= np.max(sample_bottom_theta_norm)
    color_idxs = [cmap(sample_bottom_theta_norm[t]) for t in range(len(sample_bottom_theta))]
    return(color_idxs)


def interpolate_positions(start_xy, end_xy, n_interpolation = 10):
    n_cells = start_xy.shape[0]
    ## x, y, interpolation_steps (or time)
    out_pos = np.zeros((n_cells,2,n_interpolation))
    for i in range(n_cells):
        out_pos[i,0,:] = np.linspace(start_xy[i,0],end_xy[i,0],num=n_interpolation)
        out_pos[i,1,:] = np.linspace(start_xy[i,1],end_xy[i,1],num=n_interpolation)
    return(out_pos)


def extract_frame_index_from_subject_arranged(xyt_subj_list, frame):
    out_subj_list_of_frame=[xyt_subj_list[i][:,:,frame] for i in range(len(xyt_subj_list))]
    print(out_subj_list_of_frame)
    print(len(out_subj_list_of_frame))
    return(out_subj_list_of_frame)


def get_interpolations(start_pos, end_pos, n_frames):
    inter_pos = []
    for i in range(len(start_pos)):
        ## go through each batch & collect their interpolations
        inter_pos.append(interpolate_positions(start_pos[i], end_pos[i], n_interpolation=n_frames))
    return(inter_pos)


def get_and_plot_interpolation(start_pos, end_pos, colors, n_frames=10, counter=0, leader="full_animation_", frame_counter=0, out_dir="", ymin=-.2):
    inter_pos=get_interpolations(start_pos, end_pos, n_frames)
    for f in range(n_frames):
        out_frame = ""
        if out_dir != "":
            out_frame = f"frame_{frame_counter:004}.png"
            out_frame = os.path.join(out_dir,out_frame)
        create_plots_within_stage(extract_frame_index_from_subject_arranged(inter_pos,f), colors = colors, out_frame=out_frame,ymin=ymin)
        frame_counter+=1
    return(frame_counter)


def plot_hold(in_data, colors, out_dir, frame_counter, hold_frames,ymin=-.2):
    for i in range(hold_frames):
        out_frame = f"frame_{frame_counter:004}.png"
        out_frame = os.path.join(out_dir,out_frame)
        create_plots_within_stage(in_data, colors = colors, out_frame=out_frame,ymin=ymin)
        frame_counter += 1
    return(frame_counter)


def create_animation(null, 
                     b, 
                     bs,
                     tr, 
                     tm, 
                     aligned,
                     sample_bottom_theta, 
                     n_frames_interpolate=25,
                     n_hold = 10,## number of frames to hold for each position
                     out_dir="",
                     sample_colors = None):
    if out_dir != "":
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
    ####################
    ## get the colors ##
    if sample_colors is None:
        sample_colors  = get_colors(sample_bottom_theta)
    ##
    plt.clf()
    plt.figure(figsize=(162,90))
    frame_counter  = 0
    ###
    # plot null t cell types
    frame_counter = plot_hold(all_data_null,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(all_data_null, all_data_b, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    ###
    # Add the state
    frame_counter = plot_hold(all_data_b,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(all_data_b, all_data_bs, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    ## 
    # Add Tr
    frame_counter = plot_hold(all_data_bs,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(all_data_bs, all_data_tr, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    ###
    # Add Tm
    frame_counter = plot_hold(all_data_tr,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(all_data_tr, all_data_tm, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    ###
    # plot the "alignment"
    frame_counter = plot_hold(all_data_tm,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(all_data_tm, aligned, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    ###
    # Go back to the full set of biology
    frame_counter = plot_hold(aligned,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(aligned, all_data_bs, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    frame_counter = plot_hold(all_data_bs,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    return



#########################################################
def filter_for_sample(null, b, bs, tr, tm, num_sample):
    num_cells = null[0].shape[0]
    subset = np.sort(np.random.choice(np.arange(num_cells,dtype=int),size=num_sample,replace=False))
    for i in range(len(null)):
        null[i]=null[i][subset,:]
        b[i]=b[i][subset,:]
        bs[i]=bs[i][subset,:]
        tr[i]=tr[i][subset,:]
        tm[i]=tm[i][subset,:]
    return(
        null,
        b,
        bs,
        tr,
        tm,
        subset
        )


def filter_cells_from_second_batch(tm2,kept_idxs,num_samples_in_first_batch):
    start_batch_idxs = int(len(tm2)-num_samples_in_first_batch)
    for i in range(start_batch_idxs,len(tm2)):
        tm2[i]=tm2[i][kept_idxs,:]
    return(tm2)


def generic_filter(in_list, cell_subset_idxs):
    out_list = []
    for i in range(len(in_list)):
        out_list.append(in_list[i][cell_subset_idxs[i],:])
    return(out_list)


def get_flat(orig, idx):
    return(np.concatenate([(np.zeros((orig.shape[0],1))+idx),orig],axis=1))


def rearrange_lol_to_flat(lol):
    out = None
    for i in range(len(lol)):
        if out is None:
            out = get_flat(lol[i],i)
        else:
            out = np.concatenate([out,get_flat(lol[i],i)])
    out_df = pd.DataFrame(out, columns = ["batch","x","y"])
    print(out_df)
    return(out_df)


def rearange_flat_to_lol(in_flat):
    out_lol = []
    for i in range(int(max(in_flat["batch"]))+1):
        sub_df = in_flat[in_flat["batch"]==float(i)]
        out_lol.append(sub_df.iloc[:,1:].to_numpy())
    return(out_lol)


def get_aligned_rotated_x_and_pseudotime_y(local_aligned_rotation,local_pseudotime_locs):
    out_aligned_bad_pseudo = []
    for i in range(len(local_aligned_rotation)):
        out_aligned_bad_pseudo.append(deepcopy(local_aligned_rotation[i]))
        ## now change the y for it
        out_aligned_bad_pseudo[i][:,1]=deepcopy(local_pseudotime_locs[i][:,1])
    return(out_aligned_bad_pseudo)


def sigmoid(x, mag = 25):
    x = np.array(x,dtype=float)
    x = min_max_norm(x, post_shrink = 1/mag) - mag/1.85
    out_x = 1.0 / (1.0 + np.exp(-x))
    ## now map it to a negative binomial
    rand_nb = np.log2(1+np.array(np.random.negative_binomial(.05,.01,x.shape[0]),dtype=float))#+np.random.normal(0,.01,x.shape[0])
    print(out_x)
    print(rand_nb)
    out_x_mapped = get_single_ot_pair(
        np.array([out_x,out_x]).T, 
        np.array([rand_nb,rand_nb]).T
        )
    return out_x_mapped[:,0]


def create_pseudotime_var(start, in_bottom_idxs, rotation_angle):
    start = generic_filter(start, in_bottom_idxs)
    flat_coords = rearrange_lol_to_flat(start)
    plt.scatter(flat_coords.iloc[:,1], flat_coords.iloc[:,2])
    new_x, new_y = rotate(flat_coords.iloc[:,1], flat_coords.iloc[:,2], rotation_angle)
    new_x = min_max_norm(new_x)
    out_y = min_max_norm(min_max_norm(sigmoid(new_x-0.5)) + np.random.normal(0,.2,new_x.shape[0]))
    new_y = min_max_norm(new_y, post_shrink=5)
    # plt.scatter(new_x, new_y)
    # plt.scatter(new_x, out_y)
    # plt.show()
    flat_coords_rotation = deepcopy(flat_coords)
    flat_coords_rotation.iloc[:,1] = new_x
    flat_coords_rotation.iloc[:,2] = new_y
    flat_coords_pseudotime_var = deepcopy(flat_coords)
    flat_coords_pseudotime_var.iloc[:,1] = new_x
    flat_coords_pseudotime_var.iloc[:,2] = out_y
    return(
        rearange_flat_to_lol(flat_coords_rotation),
        rearange_flat_to_lol(flat_coords_pseudotime_var)
        )


def create_pseudotime_animation(
    subset_original_locs,
    subset_rotaed_locs,
    final_pseudotime_locs,
    sample_colors,
    n_frames = 40,
    n_hold = 10,
    out_dir = ""):
    plt.clf()
    plt.figure(figsize=(162,90))
    if out_dir != "":
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
    frame_counter=0
    frame_counter = plot_hold(
        subset_original_locs,
        sample_colors, 
        out_dir=out_dir, 
        frame_counter=frame_counter, 
        hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(
        subset_original_locs,
        subset_rotaed_locs,
        sample_colors,
        n_frames = n_frames, 
        out_dir=out_dir, 
        frame_counter=frame_counter)
    frame_counter = plot_hold(
        subset_rotaed_locs,
        sample_colors, 
        out_dir=out_dir, 
        frame_counter=frame_counter, 
        hold_frames = n_hold)
    frame_counter = get_and_plot_interpolation(
        subset_rotaed_locs,
        final_pseudotime_locs,
        sample_colors,
        n_frames = n_frames, 
        out_dir=out_dir, 
        frame_counter=frame_counter)
    frame_counter = plot_hold(
        final_pseudotime_locs,
        sample_colors, 
        out_dir=out_dir, 
        frame_counter=frame_counter, 
        hold_frames = n_hold)
    return


def create_transfer_animation(null, 
                     b, 
                     bs,
                     tr, 
                     tm, 
                     null2, 
                     b2, 
                     bs2,
                     tr2, 
                     tm2, 
                     n_frames_interpolate=25,
                     n_hold = 10,## number of frames to hold for each position
                     out_dir="",
                     sample_colors_1 = None,
                     sample_colors_2 = None,
                     num_sample = 150,
                     ymin=-0.2):
    plt.clf()
    plt.figure(figsize=(162,90))
    if out_dir != "":
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
    ###########################
    ## first get the subsample 
    null_orig = deepcopy(null)
    b_orig = deepcopy(b)
    bs_orig = deepcopy(bs)
    tr_orig = deepcopy(tr)
    tm_orig = deepcopy(tm)
    null, b, bs, tr, tm, kept_idxs = filter_for_sample(null, b, bs, tr, tm, num_sample=num_sample)
    ## now pull out the cells from the first batch
    ## that were warped by the second tm effects
    tm2 = filter_cells_from_second_batch(tm2,kept_idxs,len(null))
    ###########################
    frame_counter  = 0
    ###
    # plot null t cell types
    frame_counter = plot_hold(null2,sample_colors_2, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold, ymin=ymin)
    frame_counter = get_and_plot_interpolation(null2, b2, sample_colors_2, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter, ymin=ymin)
    ###
    # Add the state
    frame_counter = plot_hold(b2,sample_colors_2, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold, ymin=ymin)
    frame_counter = get_and_plot_interpolation(b2, bs2,sample_colors_2, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter, ymin=ymin)
    ###
    # Add Tr
    frame_counter = plot_hold(bs2,sample_colors_2, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold, ymin=ymin)
    frame_counter = get_and_plot_interpolation(bs2, tr2, sample_colors_2, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter, ymin=ymin)
    ###
    frame_counter = plot_hold(tr2,sample_colors_2, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold, ymin=ymin)
    frame_counter = plot_hold(tr2+tr,sample_colors_2+sample_colors_1, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold, ymin=ymin)
    frame_counter = get_and_plot_interpolation(tr2+tr, tm2, sample_colors_2+sample_colors_1, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter, ymin=ymin)
    ###
    # Add Tm #### TODO: pick up here
    # # plot the "alignment"
    # frame_counter = plot_hold(tm2,sample_colors_2, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    # frame_counter = get_and_plot_interpolation(tm2, aligned, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    # ###
    # # Go back to the full set of biology
    # frame_counter = plot_hold(aligned,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    # frame_counter = get_and_plot_interpolation(aligned, all_data_bs, sample_colors, n_frames = n_frames_interpolate, out_dir=out_dir, frame_counter=frame_counter)
    # frame_counter = plot_hold(all_data_bs,sample_colors, out_dir=out_dir, frame_counter=frame_counter, hold_frames = n_hold)
    return









###################################################################
top_out_dir = "data/functional_framework_plots/"
if not os.path.isdir(top_out_dir):
    os.mkdir(top_out_dir)

sample_bottom_theta=[-.25,-.15,-.13,-.1,0,.03,.13,.2]
sample_colors = get_colors(sample_bottom_theta)
plt.close("all")
np.random.seed(123456)#314159
all_data_null, all_data_b, all_data_bs, in_bottom = make_data(1000,sample_bottom_theta)

np.max(all_data_bs[0][in_bottom[0],:])


bs_rotation, pseudotime_locs = create_pseudotime_var(all_data_bs, in_bottom, -1.75)

all_data_tr, tr_x_deform, tr_y_deform, tr_mag = apply_random_field(deepcopy(all_data_bs),
                                                           shrinking_factor = .2,
                                                           smoothness=25,
                                                           leader = "tr",
                                                           out_dir= top_out_dir)
all_data_tm, tm_x_deform, tm_y_deform, tm_mag = apply_random_field(deepcopy(all_data_tr), 
                                                           shrinking_factor = .15,
                                                           smoothness = 1.5,
                                                           same_field_to_all=False,
                                                           leader = "tm",
                                                           out_dir = top_out_dir)


aligned = get_generic_alignment(deepcopy(all_data_tm))

# get_and_plot_interpolation(all_data_tm, rand, sample_colors)

##################################################################
##################################################################
## plot pseudotime

# get_and_plot_interpolation(all_data_tm, rand, sample_colors)
bottom_bs_filtered = generic_filter(deepcopy(all_data_bs),in_bottom)
good_pseudotime_dir = "data/functional_framework_plots/good_pseudotime/"

plt.close("all")
create_pseudotime_animation(
    bottom_bs_filtered,
    bs_rotation,
    pseudotime_locs,
    sample_colors,
    out_dir=good_pseudotime_dir)


plt.close("all")

# get_and_plot_interpolation(
#     bottom_bs_filtered,
#     bs_rotation,
#     sample_colors,
#     n_frames = 5, out_dir="", frame_counter=0)


# get_and_plot_interpolation(
#     bs_rotation,
#     pseudotime_locs,
#     sample_colors,
#     n_frames = 5, out_dir="", frame_counter=0)

##################################################################
##################################################################
## show why you can't do it with the aligned data



##################################################################
##################################################################
aligned_rotation, alignedpseudotime_locs = create_pseudotime_var(deepcopy(aligned), in_bottom, -1.75)
aligned_filtered = generic_filter(deepcopy(aligned),in_bottom)

aligned_bad_pseudo = get_aligned_rotated_x_and_pseudotime_y(
    deepcopy(aligned_rotation),
    deepcopy(pseudotime_locs)
    )


bad_pseudotime_dir = "data/functional_framework_plots/bad_pseudotime/"

plt.close("all")
create_pseudotime_animation(
    aligned_filtered,
    aligned_rotation,
    aligned_bad_pseudo,
    sample_colors,
    out_dir=bad_pseudotime_dir)

plt.close("all")



##################################################################
##################################################################

#get_and_plot_interpolation(all_data_tm, aligned, sample_colors)



plt.close("all")
typical_protocol_dir = os.path.join(top_out_dir,"typical_protocol/")
if not os.path.isdir(typical_protocol_dir):
    os.mkdir(typical_protocol_dir)


animation_dir = os.path.join(typical_protocol_dir,"functional_framework_animation")
create_animation(deepcopy(all_data_null), 
                     deepcopy(all_data_b), 
                     deepcopy(all_data_bs), 
                     deepcopy(all_data_tr), 
                     deepcopy(all_data_tm), 
                     deepcopy(aligned),
                     sample_bottom_theta, 
                     40,
                     out_dir = animation_dir)
plt.close("all")
##################################################################


###################################################################
###################################################################
###################################################################
########## NOW PLOT THE MUCH MORE IDEAL PROTOCOL ##################
###################################################################
###################################################################
top_out_dir = "data/functional_framework_plots/no_tr_one_tm/"
if not os.path.isdir(top_out_dir):
    os.mkdir(top_out_dir)

sample_bottom_theta=[-.25,-.15,-.13,-.1,0,.03,.13,.2]
plt.close("all")
np.random.seed(123456)#314159
all_data_null, all_data_b, all_data_bs, in_bottom = make_data(1000,sample_bottom_theta)
all_data_tr, tr_x_deform, tr_y_deform, tr_mag = apply_random_field(deepcopy(all_data_bs),
                                                           shrinking_factor = .01,
                                                           smoothness=500,
                                                           leader = "tr",
                                                           out_dir= top_out_dir)
all_data_tm, tm_x_deform, tm_y_deform, tm_mag = apply_random_field(deepcopy(all_data_tr), 
                                                           shrinking_factor = .15,
                                                           smoothness = 1.5,
                                                           same_field_to_all=True,
                                                           leader = "tm",
                                                           out_dir = top_out_dir)


aligned = get_generic_alignment(deepcopy(all_data_tm))





plt.close("all")
animation_dir = os.path.join(top_out_dir,"functional_framework_animation")
create_animation(deepcopy(all_data_null), 
                     deepcopy(all_data_b), 
                     deepcopy(all_data_bs), 
                     deepcopy(all_data_tr), 
                     deepcopy(all_data_tm), 
                     deepcopy(all_data_tm),## don't show the alignment
                     sample_bottom_theta, 
                     40,
                     out_dir = animation_dir)
plt.close("all")


###################################################################
###################################################################
###################################################################
########## NOW PLOT THE MUCH MORE IDEAL PROTOCOL ##################
#################### With a second batch ##########################
###################################################################


top_out_dir = "data/functional_framework_plots/warp_transfer/"
if not os.path.isdir(top_out_dir):
    os.mkdir(top_out_dir)


sample_bottom_theta2=[-.03,.05,.13,.15,.22,.27,.3,.33]
plt.close("all")
np.random.seed(314159)
all_data_null2, all_data_b2, all_data_bs2, in_bottom2 = make_data(1000,sample_bottom_theta2)
all_data_tr2, tr_x_deform2, tr_y_deform2, tr_mag2 = apply_random_field(deepcopy(all_data_bs2),
                                                           shrinking_factor = .01,
                                                           smoothness=500,
                                                           leader = "tr",
                                                           out_dir= top_out_dir)
all_data_tm2, tm_x_deform2, tm_y_deform2, tm_mag2 = apply_random_field(deepcopy(all_data_tr2)+deepcopy(all_data_tr), 
                                                           shrinking_factor = .15,
                                                           smoothness = 1.5,
                                                           same_field_to_all=True,
                                                           leader = "tm",
                                                           out_dir = top_out_dir)


#aligned = get_generic_alignment(deepcopy(all_data_tm))


# get_and_plot_interpolation(all_data_tm, rand, sample_colors)


#get_and_plot_interpolation(all_data_tm, aligned, sample_colors)



plt.close("all")
animation_dir = os.path.join(top_out_dir,"functional_framework_animation")


create_transfer_animation(deepcopy(all_data_null), 
                         deepcopy(all_data_b), 
                         deepcopy(all_data_bs), 
                         deepcopy(all_data_tr), 
                         deepcopy(all_data_tm), 
                     deepcopy(all_data_null2), 
                     deepcopy(all_data_b2), 
                     deepcopy(all_data_bs2), 
                     deepcopy(all_data_tr2), 
                     deepcopy(all_data_tm2), 
                     40,
                     out_dir = animation_dir,
                     sample_colors_1 = get_colors(sample_bottom_theta),
                     sample_colors_2 = get_colors(sample_bottom_theta,cmap=cm.RdYlGn),
                     ymin=-.4)
plt.close("all")



