# sc_norm_bench
This script performs benchmarking using both biologic datasets as well as simulated datasets that simulate both cell types and cell states using a linear mixture model of negative binomial distributions.

## System Requirements
Note that because our pipeline performs about 100 different scRNAseq dataset integrations, this takes up a very large amount of hard-drive space (roughly 300Gb without cross-validation, but 825Gb with it). You'll also need around 32Gb of RAM.

## R Dependencies
There are a number of dependencies that will need to be installed, listed below are the R packages:

    library("splatter")
    library("scater")
    library("Seurat")
    library("DESeq2")
    library("sva")
    library("irlba")
    library("rliger")
    library("harmony")
    library("BiocParallel")
    library("ggplot2")
    library("reshape2")
    library("pals")
    library("tidyverse")
    library("stringr")
    library("scales")
    library("entropy")
    library("NMF")
    library('ggbeeswarm')
    library("PercentMaxDiff")
    library("reshape2")
    library("igraph")
    library("Matrix")

## python3 Dependenceies
[PyMINEr](https://www.sciencescott.com/pyminer) is used for much of the automated aspects of scRNAseq analysis including highly variable gene based feature selection and unsupervised clustering. To install this dependency, type the following:

    sudo python3 -m pip install bio-pyminer
Note that you can also install this locally, with:

    python3 -m pip install bio-pyminer

## Recapitulating our results
To recapitulate our results, cd into the repository directory (wherever it is that you put it on your system), then run it.

    git clone https://bitbucket.org/scottyler892/sc_norm_bench.git
    cd sc_norm_bench
    Rscript bin/sc_norm_bench.R


Note that given the large number of benchmarks, it will take around 3-4 weeks to complete. However, progress is saved to disk, so if you have a system failure in the middle, the script will pick up where it left off.

